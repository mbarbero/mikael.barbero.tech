# https://mikael.barbero.tech

## Wrangler (Cloudflare API) commands

```
wrangler login
wrangler pages project list
wrangler pages deployment list --project-name mikael-barbero
wrangler pages publish public --project-name mikael-barbero --branch=main
```