.PHONY: deploy-preview deploy-production update-theme

deploy-preview:
	hugo --buildDrafts --buildFuture --destination public
	wrangler pages deploy public --project-name mikael-barbero --commit-dirty=true --branch=preview-from-local

deploy-production:
	hugo --gc --cleanDestinationDir --minify --destination public
	wrangler pages deploy public --project-name mikael-barbero --branch=main
	@echo ":rocket:  Production page is https://mikael.barbero.tech" | gum format --type="emoji"

update-theme:
	hugo mod get -u