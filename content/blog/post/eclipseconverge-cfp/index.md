---
title: "EclipseConverge 2017 Submissions are open"
date: 2016-11-15T10:45:00+02:00 #NOVEMBER 15, 2016 (10:45 AM)
tags:
  - CFP
  - Conference
---

The call for papers for [EclipseConverge 2017](https://www.eclipseconverge.org/na2017/) is open. It is the first step toward what ought to be another great Eclipse event. For those who may not know, Eclipse Converge is a new event for the Eclipse community. It is a one-day summit dedicated exclusively to Eclipse technologies, with the goal of allowing our North American developer community to meet and share ideas.

<!--more-->

As you may know, I am leading the program committee this year as program chair. We worked hard to make the best call for papers we possibly could. We designed the tracks carefully so that anyone near or far from the core Eclipse community will feel welcome to submit a speaking proposal about all the inspiring, innovative and interesting stuff they are doing in or with Eclipse. By the way, you can see us on the [PC page](https://www.eclipseconverge.org/na2017/programcommittee) and contact us anytime via [email](mailto:speakers@eclipsecon.org).

This year, we will have four tracks:

* **Eclipse Platforms & Runtimes**. Eclipse is an IDE and also a host for a lot of great open-source runtime technologies such as Equinox, RAP, RCP, and the Eclipse 4 Application Platform. New technologies from the vibrant Eclipse ecosystem as well as state-of-the-union talks go here.
* **Languages & Tools**. There are countless plugins and tools based on Eclipse to support developers in their daily work. Tell us about the best ones built on Eclipse, how you have built them, and/or what technologies they
* **Web & Cloud**. If you are implementing Web / JEE applications or want to share your experience with modern web frameworks such as AngularJS, Vaadin, or JSON Forms, your talk goes here. This is also the track for talks about cloud development and operation tools at Eclipse such as Orion, Che, Flux, Dirigible, or other ways to move tooling to the cloud, such as browser-based UIs for developer tools and next-generation
* **Other Cool Stuff**. Not everything will fit easily into one of these tracks. If your talk is one that’s hard to categorize, submit it here!

The submission deadline is **December 12, 2016, at 11:59 PM PST**. There’s no time for procrastination! Go ahead and [submit yours now](https://www.eclipseconverge.org/na2017/propose-session).

---

Originally published at [mikael-barbero.tumblr.com](https://mikael-barbero.tumblr.com/post/153209914535/eclipseconverge-2015-submissions-are-open) and [mikael-barbero.medium.com](https://mikael-barbero.medium.com/eclipseconverge-2017-submissions-are-open-6dfa773f0334).
