---
title: "Elevating Software Supply Chain Security: Eclipse Foundation's 2FA Milestone"
date: 2023-12-18T18:00:00+02:00
thumbnail: 2fa.png
tags:
  - opensource
  - security
  - 2fa
---

In the realm of open-source software, security of the supply chain is not just a concern—it's a crucial battleground. The Eclipse Foundation, at the forefront of this fight, has taken a decisive step with its 2023 initiative to enforce two-factor authentication (2FA) across its platforms. This move is more than a security upgrade; it's a testament to the Foundation's commitment to safeguarding the open-source software supply chain against escalating threats.

<!--more-->

The traditional reliance on password-based authentication poses a significant risk, especially in open-source software development. As highlighted in a [previous article](https://mikael.barbero.tech/blog/post/2022-11-22-2fa-for-developers/), compromised developer accounts can become conduits for malicious code, affecting not only the developers themselves but also downstream users. The **alarming 742% average annual increase in software supply chain attacks** in recent years underscores the urgency of robust security measures. Recognizing this threat, the Eclipse Foundation has championed the shift to more secure authentication methods with 2FA.

The road to comprehensive 2FA implementation was not without its challenges. One of the main hurdles was addressing misunderstandings about what 2FA entails. For example, there was confusion about [whether 2FA required hardware tokens](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1251#note_896310). Additionally, concerns arose about what actions to take in case of a loss of the second factor. The Foundation tackled these issues head-on through [repeated](https://www.eclipse.org/lists/eclipse.org-committers/msg01389.html) [communication](https://www.eclipse.org/lists/eclipse.org-committers/msg01397.html) and education, providing clear, accessible information to demystify 2FA and allay fears.

Feedback and insights from the Eclipse community were invaluable in shaping the 2FA initiative. A particularly unique challenge came from members who were [unable to use mobile phones or hardware tokens at their workplaces](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/4012) due to internal policies. To address this, the Foundation helped identify software solutions that facilitated TOTP-based 2FA, aligning with the IT requirements of these members: we were committed committed to help everybody and find practical solutions to all problems.

![2FA Bird](2fa.png)

For the Eclipse Foundation, 2FA is just one aspect of a broader vision to harden software supply chain security. Recognizing that the [first line of defense starts with developers](https://mikael.barbero.tech/blog/post/2022-11-22-2fa-for-developers/), the Foundation has positioned itself as a role model in software supply chain security. Beyond 2FA, it is actively helping its projects to better understand and communicate their dependencies through Software Bill of Materials (SBOMs), manage vulnerabilities in these dependencies, and secure their build pipelines against potential threats. These initiatives are set to continue throughout 2024.

The impact of the 2FA enforcement initiative is clearly demonstrated by the significant adoption rates within the Eclipse community. On GitHub, **91% of our committers have now enabled 2FA**, with 63% of organizations achieving complete member compliance. In 2024, we will take the final steps to fully enforce 2FA for committers on GitHub, ensuring that all organizations will exclusively have members with 2FA enabled. For projects hosted on [gitlab.eclipse.org](http://gitlab.eclipse.org/eclipse), **63% of committers have enabled 2FA**. Since December 11th, committers are required to enable 2FA upon signing in—if they haven't already—before they can proceed with any other actions.

The 2FA enforcement initiative of the Eclipse Foundation represents an essential measure in hardening the security of the open-source software supply chain of its projects. This initiative underscores the significance of shared responsibility and vigilance in cybersecurity.

---

This work was made possible thanks to the funding the Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project. 