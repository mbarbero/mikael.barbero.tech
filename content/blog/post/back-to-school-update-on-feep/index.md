---
title: "Back to school update on FEEP"
date: 2016-09-05T12:00:00+02:00
tags:
  - Eclipse IDE
  - FEEP
---

You [remember]({{< ref "/blog/post/its-feep-ing-time" >}}) the Friends of Eclipse Enhancement Program, right? It is a program that utilizes all the donations made through the Friends of Eclipse program to make significant and meaningful improvements and enhancements to the Eclipse IDE/Platform. I think it is a good time for me to provide you with an **update about what we have done in the last quarter with this program**.

<!--more-->

One of the major effort we have been focused on is the **triage of the key Eclipse Platform UI bugs**. The [bid](https://projects.eclipse.org/development_effort/triagefix-key-platformui-errors) has been awarded to [Patrik Suzzi](https://accounts.eclipse.org/users/psuzzi) and I must say that the Eclipse Platform team and the Eclipse Foundation have been delighted to work with Patrik. Since the beginning of April, he has **triaged about [400 bugs](https://bugs.eclipse.org/bugs/buglist.cgi?cmdtype=dorem&namedcmd=Platform%20TRG&remaction=run&sharer_id=78132&list_id=15948022) and [fixed](https://bugs.eclipse.org/bugs/buglist.cgi?cmdtype=dorem&list_id=15948023&namedcmd=Platform%20FIX&remaction=run&sharer_id=78132) or [contributed](https://bugs.eclipse.org/bugs/buglist.cgi?cmdtype=dorem&list_id=15948024&namedcmd=Platform%20REV-CON&remaction=run&sharer_id=78132) to fix 70 bugs in the Platform**. It granted him to become an Eclipse Platform UI committer. Congratulation!

Among others, Patrik has fixed some very annoying bugs like the [broken feedback on drag and drop of overflown tabs editor](https://bugs.eclipse.org/bugs/show_bug.cgi?id=497348) or the inclusion of an [Eclipse help search in the quick access field](https://bugs.eclipse.org/bugs/show_bug.cgi?id=459989). By the way, if you don’t know what quick access is, I urge you to [have a look at it](https://waynebeaton.wordpress.com/2016/04/06/quick-access-to-eclipse-ide-features/).

![Quick search](Vh_A02q0iAegerGf.png)

Another area I’ve been working on is the [progress monitor performance](https://bugs.eclipse.org/bugs/show_bug.cgi?id=500332). When projects would do heavy progress reporting, the reporting itself was slowing down the running task by a huge factor. Have a look at how it now runs faster and smoother now.

[![Progress monitor performance overview](https://img.youtube.com/vi/rARBZ4ncREQ/0.jpg)](https://www.youtube.com/watch?v=rARBZ4ncREQ)

Many more fixes and improvements can be done to the Eclipse IDE/Platform with this program. Obviously, the development depends on the amount of donations received. You can help improve the Eclipse Platform and make a difference. You only need to donate today!

---

Originally published at [mikael-barbero.tumblr.com](https://mikael-barbero.tumblr.com/post/149788108430/back-to-school-update-on-feep) and [mikael-barbero.medium.com](https://mikael-barbero.medium.com/back-to-school-update-on-feep-9d7acb1714fd).