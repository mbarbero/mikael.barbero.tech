---
title: "Enforcing HTTPS on the Eclipse Marketplace"
date: 2022-09-20T19:25:00+02:00
thumbnail: "marketplace.png"
tags:
  - Security
  - MITM
  - TLS
---

As stewards of the [Eclipse Marketplace](https://marketplace.eclipse.org), the Eclispe Foundation is responsible for providing a safe place for the Eclipse IDE users to download their plugins. While the Eclipse Marketplace does not host or transmit the plugins bits, it provides links to (p2) repositories containing them. Until today, there was no restriction on those links.

Beginning **December 15, 2022**, the Eclipse Marketplace will no longer support links to repositories over plain HTTP. The goal is to protect users of the Eclipse Marketplace from the main risk of plain HTTP links: [man-in-the-middle (MITM) attacks](https://infosecwriteups.com/want-to-take-over-the-java-ecosystem-all-you-need-is-a-mitm-1fc329d898fb).

<!--more-->

We will roll out this new requirement in 4 steps:

* Starting today, **September 20, 2022**, a banner will be displayed on the Eclipse Marketplace website homepage with the goal to spread awareness of the upcoming changes.
* On **October 14, 2022**, the banner will start to be displayed on all Eclipse Marketplace pages and new validation rules will be added to the Eclipse Marketplace backend. New solutions will not be allowed to use links to repositories over plain HTTP and existing solutions will not be able to edited if they still link content over plain HTTP.
* On **December 15, 2022**, all non-compliant solutions will be deactivated and won't be displayed anymore on the Marketplace website. Owners will still be able to fix those for a limited period and get them re-instated.
* On **January 30, 2023**, all deactivated solutions that have not been fixed will be permenently deleted.

Of course, we will regularly remind every owners of non-compliant solutions about the upcoming changes and the risks associated with not fixing their solutions.

If you want to follow on this work, you can subscribe for notifications on [the corresponding issue](https://gitlab.eclipse.org/eclipsefdn/security/public-tasks/-/issues/1).

![Eclipse Marketplace Logo](marketplace.png)