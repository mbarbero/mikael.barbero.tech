---
title: "On password managers, multi-factor authentication, and TOTP tokens"
date: 2022-09-21T14:00:00+02:00
thumbnail: "kristina-flour-BcjdbyKWquw-unsplash-small.jpg"
draft: true
tags:
  - Security
  - Passwords
  - Multi-Factor Authentication
---
Hard fact: passwords can be [hacked](https://haveibeenpwned.com), [guessed](https://en.wikipedia.org/wiki/List_of_the_most_common_passwords#cite_note-4), or even [phished](https://en.wikipedia.org/wiki/Phishing). It is [recommended](https://pages.nist.gov/800-63-3/sp800-63b.html) to have long and strong passwords, and to not reused them accross websites. By doing so, we, humans beings with limited memory, loose the ability to remember all of them. Thus, we have to rely on [password managers](https://bitwarden.com) to store our precious credentials.

Recommendations also promote [Multi-factor authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication) (MFA; encompassing **two-factor authentication**, or **2FA**). MFA neutralizes the risks associated with compromised passwords by requiring the user to supply more than one piece of evidence (factor), than just a password. It is considered better when the factors are of different natures:
* **Knowledge** e.g., a password or a PIN number.
* **Ownership** e.g., a security token or a key.
* **Inherence** e.g., your fingerprint or your face.

If we use a password manager, can we still consider a password a *knowledge*? Does it actually correspond to any of the nature listed above? Is it still safer to have MFA in this case? If you activate a second factor in the form a [Time-base one-time password (TOTP)](https://en.wikipedia.org/wiki/Time-based_one-time_password), is it safer to store it in a password manager beside the password or on a different device/app? Is TOTP a second factor authentication anyway?

<!--more-->

## The nature of an unknown password

An unkown password is not knowledge (sic), so what is the nature of the evidence we type in a password field? If this password is stored in a password manager, then it inherits all the natures of the factors you use to authenticate to your password managed.

![Photo by Kristina Flour on Unsplash](kristina-flour-BcjdbyKWquw-unsplash-small.jpg "Photo by [Amol Tyagi](https://unsplash.com/@tinaflour) on [Unsplash](https://unsplash.com/)")

## MFA, always MFA

Does this mean that you don't need need MFA on strong nad unique passwords stored in mnagers? Of course not! Such a password can be phished or leaked. A second factor will mitigate those risks.

## TOPT tokens in password managers

The most common ways for doing MFA, in addition to passwords, are:
* Code sent via email or SMS
* Time-based one-time password (TOTP), a short code computed from the current time and a shared secret. The shared secret is usually installed on a mobile app that generate a new code every 30 seconds for each site.
* U2F or FIDO2, a security key (generally on USB or NFC) that replies to a cryptographic challenge and used as a Ownership factor.



## Final recommendations

---

If you haven't yet, please take 5 minutes to fill out this [survey on software supply chain integrity practices](https://docs.google.com/forms/d/e/1FAIpQLSfoQj2lzGIaDJ3-uVBxble6YjaElAreTO1LLaQ7Ws2XwrosXQ/viewform)