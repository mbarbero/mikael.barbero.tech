---
title: "Eclipse Mosquitto Security Audit Has Been Completed"
date: 2023-11-28T16:00:00+02:00
thumbnail: mosquitto-logo.png
tags:
  - security
  - audit
  - opensource
---

We’re excited to announce that the Eclipse Foundation has successfully conducted a security audit for [Eclipse Mosquitto](https://mosquitto.org/), marking our fourth project audit this year. To enhance security, all Mosquitto users are urged to upgrade to the latest available version. All issues identified by the audit have been fixed in the source code.

An [Eclipse IoT](https://iot.eclipse.org/) project, Eclipse Mosquitto provides a lightweight server implementation of the [MQTT protocol](https://mqtt.org/mqtt-specification/) that is suitable for all situations, from powerful servers to embedded and low power machines. Highly portable and compatible with numerous platforms, Mosquitto is a popular choice for embedded products.

[![Threat Model](mosquitto_threat_model.jpg "[Threat Model](https://github.com/trailofbits/publications/blob/master/reviews/2023-02-eclipse-mosquitto-lightweight-threatmodel.pdf)")](https://github.com/trailofbits/publications/blob/master/reviews/2023-02-eclipse-mosquitto-lightweight-threatmodel.pdf)

Since the MQTT network could potentially be accessible to attackers, correct handling of messages in a broker and correct cryptographic operations are important to the security of the entire network.

The audit revealed a few issues in the password implementation and Dynamic Security plugin. This shows that independent review can be useful for all projects, even mature ones.

[![Full Report](mosquitto_report.jpg "[Full Report](https://github.com/trailofbits/publications/blob/master/reviews/2023-03-eclipse-mosquitto-securityreview.pdf)")](https://github.com/trailofbits/publications/blob/master/reviews/2023-03-eclipse-mosquitto-securityreview.pdf)

This open source project security audit was completed by [Trail of Bits](https://www.trailofbits.com/), an independent auditor. Like our previous three audits, this initiative was done in collaboration with the [Open Source Technology Improvement Fund](https://ostif.org/) (OSTIF) and was made possible thanks to the funding the Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project. 

## Get Involved

- Download [Eclipse Mosquitto](https://mosquitto.org/), learn how you can contribute to the project, and review their [security page](https://mosquitto.org/security/).
- Learn more about the [Eclipse Cyber Risk Initiative](https://www.eclipse.org/org/workinggroups/eclipse-cyber-risk-concept.php), and how your organization can join the effort to strengthen the open source supply chain. Please subscribe to the [ECRI mailing list](https://accounts.eclipse.org/mailing-list/eclipse-cyber-risk-initiative) to join the initiative, or to follow its progress.
