---
title: "Understanding Software Provenance"
date: 2023-12-26T16:00:00+02:00
thumbnail: 5w1h.png
tags:
  - security
  - provenance
---

In the ever-evolving landscape of open-source software development, the creation and distribution of artifacts—such as compiled binaries, libraries, and documentation—represent the tangible results of a multifaceted process. These artifacts are more than just a collection of code; they are the final product of myriad decisions, alterations, and contributions, each with its unique narrative. It's essential to grasp these narratives or the **provenance of these artifacts**, to secure the supply chain effectively. Moreover, the integrity and security of these artifacts are paramount, as they underpin the trust and reliability users expect. This post aims to demystify the concept of provenance for these released artifacts. We will delve into why a comprehensive understanding of their origins and the path they take—examined through the lens of the journalistic 5W1H (Who, What, When, Where, Why, and How)—is crucial for enhancing the security posture of an open source project's supply chain.

<!--more--> 

## Understanding Artifact Provenance

Provenance in the context of released artifacts is a narrative of origin and evolution. It's a detailed account of an artifact's lifecycle from its inception in the developer's mind to its final form when it is released to the world. This lineage includes every modification, every build, and every individual who has interacted with the artifact. A well-documented provenance is not just an academic record; it's a testament to the artifact's integrity, a shield ensuring that what users download and interact with is precisely what was intended, untainted by tampering or malicious alterations.

## Challenges in Tracking Artifact Provenance

However, maintaining a comprehensive provenance is fraught with challenges. The complexity of dependencies where each layer has its own story, the sheer volume of artifacts and the speed at which they are updated, and the diverse sources they are compiled from, all contribute to a labyrinth of information that needs to be meticulously managed. Add to this the lack of standardized tools and practices for documenting and verifying provenance, and the task can seem Herculean. Yet, these challenges are not insurmountable barriers but rallying calls for robust solutions, for the security and reliability of the software supply chain hinge on this very capability.

## Provenance information

If we consider what the provenance information of released artifacts should comprise, it's akin to the outcome of any solid journalistic work: it should address the 5W1H (What, Who, Why, When, Where, and How) questions. At a fundamental level, the answers to these questions should be as follows.

* The **What** concerns identifying the released artifacts themselves, giving them an identity through an unambiguous identifier (e.g., using the [Common Platform Enumeration (CPE)](https://en.wikipedia.org/wiki/Common_Platform_Enumeration) scheme or a [Package URL](https://github.com/package-url/purl-spec)). It can also cover the licenses of the artifacts, a list of the artifact's dependencies, their respective licenses, and how they have been retrieved, among other things. This is more commonly known as a Software Bill of Materials (SBOM) and can be considered a part of the provenance information to be released with an artifact. Understanding the 'What' means having a clear, auditable trail of the components that form the backbone of your software, enabling you to assess, manage, and secure your software supply chain effectively.
* The **Who** can be as straightforward as identifying who triggered the release (or the build of the release) of the artifacts. It might also extend to include additional information about who contributed to the code included in this release, whether from the project's inception or since the last release. Details regarding any signed [Contributor License Agreement](https://en.wikipedia.org/wiki/Contributor_License_Agreement) (CLA) or accepted [Developer Certificate of Origin](https://en.wikipedia.org/wiki/Developer_Certificate_of_Origin) (DCO) by contributors can also be incorporated. Knowing who contributed what aids in tracking changes, auditing contributions, and most importantly, ensuring that only trustworthy code is incorporated into your projects.
* The **Why** pertains to understanding the reason behind the release: is it to fix a security vulnerability? Is it a scheduled release following the regular cadence? It might also involve tracking why a particular library was updated. As such, the release notes can be considered a (non-structured) part of the provenance information in this context. This aspect of provenance is about context and rationale, which is crucial for assessing the impact of changes on the overall security and functionality of your software.
* The **When** is straightforwardly about keeping track of the time of the release, to anchor it in a broader historical context. It can also involve recording the timing of the various contributions made prior to the release.
* The **Where** concerns tracing the locations of the various components that led to the released artifact. Where was the code developed and stored? Was it in a secure, trusted repository, or did it come from a less reliable source? Where was it built? Knowing these details can be the difference between a secure application and a vulnerable one. Coupled with the answer to the *When*, this mirrors the journalistic approach of establishing timelines and locations, helping you create a more comprehensive narrative of your software's development and enhancing security and control over your project's lifecycle.
* **How** relates to the methods, tools, and practices used to track and verify the origins of your code. It encompasses the mechanisms you implement to ensure that every line of code can be traced back to its source, thus ensuring integrity and reliability. This not only refers to the build pipelines and toolchains used to build and release the artifacts but also includes information about software development best practices such as code review, branch protections, secret scanning, and more.

![5w1h](5w1h.png)

While the full details of implementing software provenance attestation will be covered in a future post, all this information can already be delivered to downstream users of your project in a simple text file, for example, in the form of `buildinfo` files. Although not exhaustive, `buildinfo` files are a testament to the commitment to transparency and security, serving as a foundational element for more advanced tools and practices.

## The Importance of Artifact Provenance for Security

The narrative of provenance is critical for security. In a world where the threat landscape is as vast as it is vicious, the lack of provenance can lead to severe breaches. Compromised artifacts, malicious code insertions, and other vulnerabilities are not just theoretical risks; they are stark realities. A robust provenance framework is not just a defensive mechanism; it's a foundational pillar in building a secure, trustworthy supply chain. To enhance the security posture of its projects, understanding and implementing provenance practices is not an option; it's an imperative.

## How to trust provenance data?

Trusting provenance data generated during the build process is a commendable start. However, recognizing its limitations is crucial for establishing a more robust system of trust

### Integrity of Build-Generated Provenance

The integrity of build-generated provenance is inherently fragile. It's as secure as the environment in which it's stored and the transport methods used to deliver it. Imagine if a malicious actor gains access to the storage backend or intercepts the transport protocols; they could alter the provenance data, rendering it unreliable. A common countermeasure involves signing the provenance files or data. Digital signatures provide an additional layer of trust by making any tampering with the provenance data after its creation detectable. However, this step, while beneficial, is not a complete solution.

### Vulnerability of the Build Script

Another critical aspect to consider is the vulnerability of the build script itself. If the build pipeline is compromised, then so is the provenance it generates, whether signed or not. A compromised script might produce misleading information, feeding false data into what should be a trusted record. This scenario underscores a crucial realization: to genuinely trust the provenance data, the responsibility for generating it should shift away from the build pipeline to the build platform.

### The Shift in Responsibility

By making the build platform responsible for this task and having it sign the generated data, we create a system where the provenance is not only more resistant to tampering but also inherently more trustworthy. The build platform, ideally, is indeed in a unique position to observe and record the build process. It has access to all the information needed to generate accurate provenance data. This shift doesn't eliminate the risk of compromise, but it does mean that any tampering with the build pipeline won't affect the integrity of the provenance data we rely on.

### Securing the Build Platform

It's important to note that this approach is not a silver bullet. The build platform itself can be compromised, and securing it is a complex task that goes beyond the scope of this discussion. However, it's an essential consideration for a truly trustworthy system. Even with a secured build platform, the environment generating the provenance data must also be secure to genuinely trust the data's integrity.

In conclusion, while build-generated provenance is a valuable first step, it's essential to be aware of its limitations. Shifting the responsibility to the build platform and securing that platform are critical moves towards a more trustworthy and resilient system. However, remember that in the realm of security, no solution is absolute. Each layer of trust we add is a step towards a more secure ecosystem, but vigilance and ongoing improvement are always necessary.

## Closing notes

As we conclude our exploration of software provenance through the detailed lens of the 5W1H framework, it's clear that this is not merely an exercise in compliance or best practices. It's a fundamental shift in how we approach software development and security. Understanding the 'Who,' 'What,' 'When,' 'Where,' 'Why,' and 'How' of your artifacts isn't just about enhancing security—it's about instilling a culture of transparency and excellence.

The journey we've outlined is challenging, with numerous complexities and hurdles. However, the path to a secure and reliable software supply chain is not only necessary but also attainable with the right mindset and tools. Adopting a provenance-first approach is a paradigm shift. It means engraining the tracking and verification of the origin and journey of artifacts into the very fabric of the development and release process. It's about integrating provenance tracking into the build process, adopting tools that automate and standardize provenance documentation, and fostering a community culture where knowledge, tools, and best practices are shared freely and openly. 

As we look forward to diving into the practicalities of implementing a robust software provenance strategy in our next installment, remember that your engagement and continuous learning are vital. The principles and practices discussed here are just the beginning. With a blog post about the [Supply-chain Levels for Software Artifacts (SLSA) framework](https://slsa.dev/) on the horizon, we will have the guidelines and tools at our disposal to prevent tampering, improve integrity, and secure our packages and infrastructure.

We invite you to not just read but actively participate in shaping the future of software provenance. Join us and the Eclipse Foundation community in [discussing](https://accounts.eclipse.org/mailing-list/eclipse-cyber-risk-initiative) and advancing these crucial topics. Your insights, experiences, and commitment are key to driving change and fostering a more secure digital world.

Together, let's embrace the provenance-first mindset and lead the charge towards a future where software development is synonymous with security, transparency, and trust.