---
title: "Eclipse Foundation Security Statement: JARsigner Abuse by Malicious Actors"
date: 2025-02-21T09:15:00+02:00
thumbnail: thumbnail.jpg
tags:
  - security
  - abuse
  - dll
  - eclipse-foundation
---

[Recent](https://thehackernews.com/2025/02/cybercriminals-use-eclipse-jarsigner-to.html) [reports](https://asec.ahnlab.com/en/84574/) indicate that cybercriminals are exploiting the **Windows DLL side-loading** technique using the legitimate `jarsigner.exe` executable to propagate malware. This binary is commonly included in Java distributions such as Eclipse Temurin, which is also bundled with the Eclipse Integrated Development Environment (IDE). This has understandably raised concerns about the role of our software and whether the Eclipse Foundation or its projects bear any responsibility.

As the **Head of Security at the Eclipse Foundation**, I want to clarify the situation, explain DLL side-loading, and reaffirm our commitment to security and collaboration with the community. My goal is to provide a clear understanding of both the technical aspects of this misuse and our approach to maintaining a secure ecosystem.

## Understanding the Situation

**Dynamically Loaded Libraries (DLL) side-loading**, or **DLL hijacking**, is a [well-documented](https://attack.mitre.org/techniques/T1574/002/) Windows vulnerability. It occurs when an application like `jarsigner.exe` starts up and Windows loads required DLLs. It is important to note that this attack can exploit any other legitimate, signed binary that loads DLLs, as seen in [multiple](https://cloud.google.com/blog/topics/threat-intelligence/apt41-initiates-global-intrusion-campaign-using-multiple-exploits/) past [security](https://research.checkpoint.com/2020/naikon-apt-cyber-espionage-reloaded/) [exploits](https://securelist.com/the-tetrade-brazilian-banking-malware/97779/). If an attacker places a malicious DLL in a directory prioritized by Windows' search order, the system may load the rogue library instead of (or alongside) the legitimate one.

The sequence of events leading to malware infection unfolds as follows:

1. The victim downloads a ZIP file from a rogue website or a malicious email, often with an enticing name such as _Employee Performance Report and Termination List.zip_, as [initially reported by AhnLab](https://asec.ahnlab.com/en/84574/).
2. The victim opens or extracts the contents of the ZIP file.
3. Executing the (_renamed_) `.exe` file triggers the infection. In the [initial report](https://asec.ahnlab.com/en/84574/), the executable attempted to masquerade as a regular document, though its name—`Documents2012.exe`—was already suspicious.

In this case, the **Eclipse Temurin distribution of `jarsigner.exe` remains unmodified and legitimate**. It is merely renamed and packed within the ZIP file distributed by malicious actors. The attack relies on an attacker-supplied DLL placed alongside the trustworthy executable, not on any flaw within `jarsigner.exe` itself.

## Why This Does **Not** Implicate Eclipse Temurin or the Eclipse Foundation

The misuse of `jarsigner.exe` stems from Windows' DLL loading behavior, not a vulnerability in Eclipse Temurin. The technique affects countless Windows applications and does not reflect a security flaw in Eclipse Foundation software.

**There is no evidence of compromise within the Eclipse Foundation’s infrastructure, Temurin build systems, or projects**—not that an attacker would need any. Attackers are simply leveraging a **legitimate, signed binary** post-distribution by bundling it with malicious files.

## Our Response

Although this is **not an Eclipse Foundation software vulnerability**, security remains our top priority. We actively engage with project teams, security researchers, and downstream distributors to share information and mitigate risks. Even when an issue originates outside our control, we take steps to help users protect themselves and their organisations.

We continuously monitor security advisories and threat intelligence to detect potential misuse. When relevant, we update our documentation with best practices to help users secure their environments. If packaging or configuration adjustments can reduce DLL hijacking risks, we collaborate with downstream maintainers to implement them.

We strongly encourage Windows users and organisations to adopt **proven defenses** against DLL side-loading, including:

* **Application whitelisting** to restrict execution of unauthorised binaries.  
* **Code integrity policies** to enforce signed DLL execution.  
* **Strict application control policies** using tools like Windows AppLocker.  
* **File integrity monitoring** to detect unexpected DLLs near trusted executables.  
* **Principle of least privilege** restricting write access to application directories to prevent attackers from placing malicious DLLs.

## Looking Ahead

The Eclipse Foundation remains committed to open collaboration and secure software development. While DLL side-loading is a Windows OS issue—not a flaw in Eclipse Temurin, the Eclipse IDE or `jarsigner.exe`—we take a responsible, transparent approach to securing the software supply chain. Specifically, we provide guidance to all Eclipse Foundation projects as needed, including best practices for hardening executables against malicious actors

We appreciate the security researchers and organisations who brought attention to this issue. By clarifying the technique and its risks, we aim to help organisations enhance their defenses against such threats.

Attackers will continue to misuse legitimate software. While Eclipse Temurin’s `jarsigner.exe` has been targeted in this case, the real issue lies in Windows' DLL search order, not a vulnerability in our software. 

As always, we remain committed to supporting our users, collaborating with security professionals, and taking proactive measures to safeguard the integrity of the Eclipse ecosystem.

For security concerns or questions, contact our [Security Team](https://www.eclipse.org/security/#report-a-vulnerability).

### References & Further Reading

* [The Hacker News: Cybercriminals Use Eclipse JARsigner to Evade Detection](https://thehackernews.com/2025/02/cybercriminals-use-eclipse-jarsigner-to.html)  
* [AhnLab ASEC Blog: Cybercriminals Abusing Eclipse JARsigner](https://asec.ahnlab.com/en/84574/)  
* [MITRE ATT\&CK: T1574.002 \- DLL Search Order Hijacking](https://attack.mitre.org/techniques/T1574/002/)  
* [Eclipse Temurin / Adoptium Project](https://adoptium.net/)  
* [Eclipse Foundation Security Policies](https://www.eclipse.org/security/)
