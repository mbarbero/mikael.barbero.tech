---
title: "I’m going to Devoxx US, and you should go as well!"
date: 2017-03-07T10:00:00+02:00
tags:
  - Conference
---


For the very first time, a Devoxx conference is happening in the [USA, in San Jose, CA](http://devoxx.us/). It starts on **March 21, 2017** and is 3 days long. Devoxx conferences are famous in Europe (organized in [Belgium](https://devoxx.be/), [France](https://devoxx.fr/), [UK](http://www.devoxx.co.uk/), [Poland](http://devoxx.pl/), and [Morocco](http://devoxx.ma/)) for their high quality talks from amazing speakers. They are also very high rated because it is organized **by developers for developers**. Talks are all highly technical and the required experience from the targeted audience ranges from beginners to experts. So, with more than 200 sessions (chosen from 750 submissions!), everyone is able to craft its very own personal conference schedule. Do not trust me, [check the program](http://cfp.devoxx.us/2017/talks) by yourself!

<!--more-->

![Devoxx Belgium 2015](22947350265_f557f39b4b_c.jpg "https://www.flickr.com/photos/bejug/22947350265/in/album-72157658768130493/")

And don’t be afraid if you miss a talk because you’ve met this incredible programer your were looking for to talk about this pull request of yours. All sessions are recorded and they are published on the [Devoxx YouTube channel](https://www.youtube.com/channel/UCCBVCTuk6uJrN3iFV_3vurg) shortly after the event. Devoxx conferences are also about networking and hacking, so everything is done to let you embrace the experience!

Here is my personal selection of talks. I know I won’t be able to attend to all of them, but it shows the wide variety of topics you can learn about by coming to Devoxx US.

* [5 JVM Languages in 50 minutes](http://cfp.devoxx.us/2017/talk/TPA-9716/5_JVM_Languages_in_50_minutes) by Joe Kutner. A crash course about Clojure, Scala, JRuby, Groovy and Kotlin. I wish Ceylon was cited instead of JRuby ;)
* [Deep learning with tensorflow for multi-scale modeling of cancer patients](http://cfp.devoxx.us/2017/talk/WLQ-7821/Deep_learning_with_tensorflow_for_multi-scale_modeling_of_cancer_patients) by Olivier Gevaert
* [Rust for Java developers](http://cfp.devoxx.us/2017/talk/QKL-2419/Rust_for_Java_developers) by Hanneli Tavante
* [your next JVM: Panama, Valhalla, Metropolis](http://cfp.devoxx.us/2017/talk/YZQ-4043/your_next_JVM:_Panama,_Valhalla,_Metropolis) by Jon Rose
* [New Computer Architectures : Explore Quantum Computers & SyNAPSE neuromorphic chips](http://cfp.devoxx.us/2017/talk/VUB-9615/New_Computer_Architectures_:_Explore_Quantum_Computers_&_SyNAPSE_neuromorphic_chips) by Peter Wagget
* [55 New Features In JDK 9](http://cfp.devoxx.us/2017/talk/FLS-8924/55_New_Features_In_JDK_9) by Simon Ritter
* [Architecting for Failures in Microservices: Patterns and Lessons Learned](http://cfp.devoxx.us/2017/talk/AGA-7288/Architecting_for_Failures_in_Microservices:_Patterns_and_Lessons_Learned) by Bhakti Mehta
* [Developing Java Applications with the Eclipse IDE, Neon Edition](http://cfp.devoxx.us/2017/talk/RNK-9521/Developing_Java_Applications_with_the_Eclipse_IDE,_Neon_Edition) by Wayne Beaton and Gunnar Wagenknecht
* [The hardest part of microservices: your data](http://cfp.devoxx.us/2017/talk/MWF-9136/The_hardest_part_of_microservices:_your_data) by Christian Posta
* [gRPC 101 for Java Developers — building fast and efficient microservices](http://cfp.devoxx.us/2017/talk/MNS-9892/gRPC_101_for_Java_Developers_-_building_fast_and_efficient_microservices) by Ray Tsang
* [Developing for the Rest of the World](http://cfp.devoxx.us/2017/talk/VPN-6094/Developing_for_the_Rest_of_the_World) by Eric Frohnhoefer
* [Docker Swarm or Kubernetes — Pick your framework!](http://cfp.devoxx.us/2017/talk/TMV-3761/Docker_Swarm_or_Kubernetes_–_Pick_your_framework!_) by Arun Gupta
* [The Rise of Cloud Development — The Growth and Future of Eclipse Che — Next Generation Eclipse IDE](http://cfp.devoxx.us/2017/talk/BJF-4911/The_Rise_of_Cloud_Development_-_The_Growth_and_Future_of_Eclipse_Che_-_Next_Generation_Eclipse_IDE) by Tyler Jewell
* [Eclipse Platform News — The return of the IDE](http://cfp.devoxx.us/2017/talk/ZPD-0169/Eclipse_Platform_News_-_The_return_of_the_IDE) by Lars Vogel

What are you waiting for? Devoxx US starts in two weeks now! Go to the **[registration page](https://devoxx.us/registration/)** and get a pass now! If you’re not from the Valley, have a look at [San Jose hotels with group rates](https://devoxx.us/lodging/) for Devoxx US.

[![See you soon](https://img.youtube.com/vi/lc3YlO-us-E/0.jpg "https://www.youtube.com/watch?v=lc3YlO-us-E")](https://www.youtube.com/watch?v=lc3YlO-us-E)

---


![Eclipse Converge](jMC511AQVK7Ct6A6Usk6FQ.png)

On **Monday, March 20, 2017 an Eclipse dedicated event** is co-located with Devoxx US. If you want to know everything happening in the Eclipse ecosystem, join us for [Eclipse Converge 2017](https://www.eclipseconverge.org/na2017/) (special rates apply for combined Devoxx US + Eclipse Converge passes).

---

Originally published at [mikael-barbero.medium.com](https://mikael-barbero.medium.com/going-to-devoxx-us-and-you-should-go-as-well-1af00158c0e2).