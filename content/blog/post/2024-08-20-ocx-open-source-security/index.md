---
title: "Exploring the Future of Open Source Security at OCX 2024"
date: 2024-08-20T16:00:00+02:00
thumbnail: OCX-Color-Stack.png
tags:
  - security
  - conference
  - ocx
---

In the fast-paced world of software development, open source has emerged as a catalyst for innovation. But with this rapid growth comes an equally crucial responsibility: security. As open source continues to reshape the digital landscape, ensuring robust security measures is no longer optional; it's essential. That’s why [Open Community Experience](https://www.ocxconf.org/event/2024/summary) (OCX) is placing a strong emphasis on the latest advancements in open source security.

<!--more-->

### What Is OCX 2024?

OCX 2024 is a conference taking place on 22-24 October in Mainz, Germany, where the future of open source is actively shaped. Held at [Halle 45](https://www.ocxconf.org/event/2024/websitePage:18988bc1-3831-4549-8312-76a663222237), a cultural and event venue with a rich history, the event brings together thought leaders, developers, and tech enthusiasts from around the globe. With a focus on innovation, collaboration, and community, OCX 2024 offers an environment where you can learn, network, and contribute to the ongoing evolution of open source technologies.

OCX 2024 will feature multiple tracks of sessions, including embedded IoT and edge, open source security, open technologies, and open source best practices.

![OCX 24](OCX-Color-horizontal.png)

### A Deep Dive into Open Source Security

The security track at OCX 2024 is packed with sessions that address the most pressing challenges and opportunities in open source security. Here’s a sneak peek at what the security track has in store:

* [**Enhancing Software Supply Chain Security: Approaches to Software Composition Analysis**](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:26bc9d91-446a-49e1-9f08-8fcc4680073e?session=8e7d8fe2-8b52-4254-a71d-0dfb95837bcf\&shareLink=true)**:** Laura Bottner from Mercedes-Benz Tech Innovation will discuss strategies for improving software composition analysis, offering practical insights into using open source tools to detect vulnerabilities and secure the software supply chain.  
* [**The Future of Cybersecurity, Today: Free and Open Source Tools for CRA Compliance for SMEs**](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:26bc9d91-446a-49e1-9f08-8fcc4680073e?session=62a4594d-53c4-4158-bc1a-412e52610495\&shareLink=true): Join Philippe Ombredanne and myself to learn how small and medium enterprises (SMEs) can leverage free and open source tools to meet Cyber Resilience Act (CRA) compliance, without extensive resources.  
* [**Navigating a Security Audit, the Insights, Challenges, Experiences and Lessons Learnt**](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:26bc9d91-446a-49e1-9f08-8fcc4680073e?session=aa731871-80d6-4333-8539-7dce47ae8ee1\&shareLink=true)**:** Scott Fryer from Red Hat will offer a comprehensive guide on navigating security audits, from preparation to execution, and follow-up actions. This session is designed to demystify the audit process and arm you with the knowledge to succeed.  
* [**Actions in the Wild: Usability and Ease-of-Use of Open Source Security Tools**](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:26bc9d91-446a-49e1-9f08-8fcc4680073e?session=1f1c4864-f2ed-4f7e-b17e-6a5e552c7ba6\&shareLink=true): Rohan Krishnamurthy from ZF Group will explore the usability and effectiveness of open source security tools in real-world scenarios, highlighting the importance of choosing the right tools for continuous monitoring and vulnerability management.

### Beyond Security: What Else Can You Expect at OCX?

This year, OCX expands its horizons with collocated events that cater to the diverse and dynamic open source community:

* [**Open Community for Java (OCJ)**](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:77f60b73-336c-43be-827d-8d8fcb1aaf64)**:** Java developers will find a wealth of knowledge and insights at OCJ, which covers everything from Jakarta EE and Adoptium to the latest in open source Java technologies.  
* [**Open Community for Automotive (OCA)**](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:d99cc86b-2392-44fe-a893-2902195e5f2f)**:** Dive into the future of automotive tech and discover how open source software is fueling innovation in this fast-evolving field at OCA.

### Register now!

Make sure to [register](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:6650e052-a407-44ce-a754-b31ffba18333) so you don’t miss a [full program](https://www.ocxconf.org/event/778b82cc-6834-48a4-a58e-f883c5a7b8c9/websitePage:26bc9d91-446a-49e1-9f08-8fcc4680073e?categories=014d1ffe-d8a0-43d5-a54a-720b6dcdf37e) of technical talks, community activities, and networking opportunities. Get your tickets before 23 September to benefit from a discounted rate.

Want to learn more about the event? Visit [ocxconf.org](http://ocxconf.org) and follow [@EclipseCon](https://x.com/EclipseCon) and [@ocxconference](https://x.com/ocxconference) on social media.

I’m looking forward to seeing you in Mainz this year!
