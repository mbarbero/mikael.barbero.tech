---
title: "Chromium / Eclipse SWT integration"
date: 2017-07-04T10:00:00+02:00
tags:
  - Eclipse IDE
  - FEEP
---

> Key takeaways:
>
> Do you want to see a Chromium based SWT Browser implementation? [Please donate](https://www.eclipse.org/donate/) (or [reach out to me](mailto:mikael.barbero@eclipse-foundation.org) if you want to do corporate donations) and the Eclipse Foundation will [make it happens](https://docs.google.com/document/d/1xpLxquAZd6SGDK7XvYHoj2g_PBnnBX8SFhgwIJPv7lg/edit) via the [Friends of Eclipse Enhancement Program (FEEP)](https://www.eclipse.org/contribute/dev_program.php).

Browser support in SWT has always been a complicated story. By default (meaning without any hint from the application developers and the users), SWT relies on “native” renderers (Internet Explorer on Windows, WebKit on macOS and WebKitGTK+ or Mozilla/XULRunner on Linux). While supporting different rendering of pages in the Web is common, it’s annoying when you develop a desktop application where the Browser component is used to render things that Web technologies can do better than SWT (CSS, SVG, WebGL, etc.). Not only that, but you would expect high performance from the renderer for such usage.

To mitigate these discrepancies, developers can provide some hints to the SWT framework about which renderer (WebKit or Mozilla/XULRunner) it should use. Unfortunately, XULRunner is deprecated and is no longer [built/shipped by the Mozilla team](https://groups.google.com/forum/?_escaped_fragment_=msg/mozilla.dev.platform/_rFMunG2Bgw/C-4PcHj9IgAJ#!msg/mozilla.dev.platform/_rFMunG2Bgw/C-4PcHj9IgAJ). WebKit is staying, but it brings its own couple of issues when one tries to use it on all platforms:

* On Windows, it requires users to have Safari installed. As you may know, [Apple has silently killed Safari on Windows](http://appleinsider.com/articles/12/07/25/apple_kills_windows_pc_support_in_safari_60) back in 2012. Moreover, Safari was only available on 32 bits Windows systems. All in all, you don’t really have a choice: you need to rely on the Internet Explorer renderer when using Windows.
* On Linux, it can be difficult to setup a proper working combination of WebKitGTK and GTK+. It is especially true for older Eclipse versions on Linux distros which change GTK+ internals (hello Ubuntu!).
* On macOS, it just works™, as expected (it would have been a surprise where the system renderer is WebKit itself).

For more details on the compatibility of all platforms, you should read the [Browser section of the SWT FAQ](https://www.eclipse.org/swt/faq.php#whatisbrowser).

One solution that has been studied further is implementing the SWT Browser widget on top of the JavaFX browser component (WebView). Unfortunately, it has some compatibility issues with GTK+ versions. Furthermore, it is [reported to be slow](https://stackoverflow.com/a/10763261) and to have [trouble rendering modern HTML pages](https://stackoverflow.com/a/38860579) due to the usage of an old version of WebKitGTK.

So, [here comes the idea](https://bugs.eclipse.org/bugs/show_bug.cgi?id=405031) to embed [Chromium](https://www.chromium.org/) (the open source project behind the Google Chrome browser). It is available on all major platforms (Windows, Linux and macOS), it is known for its high performance and high quality rendering. On top of that, most of the Chromium packaging for embedding is done by the [Chromium Embedded Framework (CEF)](https://bitbucket.org/chromiumembedded/cef). CEF is quite successful and is used by some famous applications like [Spotify](https://spotify.com/), [Steam Client](http://store.steampowered.com/), and [Evernote](http://evernote.com/). It is also the foundation of the [Electron project](https://electron.atom.io/) (extracted from the text editor Atom). It is so successful and widespread that the Mozilla Foundation’s new renderer ([Servo](https://servo.org/)/[Quantum](https://wiki.mozilla.org/Quantum)) [has adopted the CEF API](https://blogs.s-osg.org/servo-the-embeddable-browser-engine/) for developers to embed it.

There have been several attempts to bring CEF to Java and SWT, but none were really successful. The [Eclipse Foundation](https://www.eclipse.org/org/) has been looking for talented developers to take over these efforts and to make it happen. Thanks to the [Friends of Eclipse Enhancement Program](https://www.eclipse.org/contribute/dev_program.php), a [call for bidding has been created](https://projects.eclipse.org/development_effort/implement-swtchromium-integration) in early 2017. The awesome people from [Make Technology](http://www.wemaketechnology.com/) have replied to it and demonstrated that they have the skills to do it. They already have a working prototype in the following GitHub repositories:

* https://github.com/maketechnology/cefswt
* https://github.com/maketechnology/cefrust

They also have a [rock solid plan](https://docs.google.com/document/d/1xpLxquAZd6SGDK7XvYHoj2g_PBnnBX8SFhgwIJPv7lg/edit?usp=sharing) and they have created umbrella bugs for each milestone of this work:

* https://bugs.eclipse.org/bugs/show_bug.cgi?id=516849
* https://bugs.eclipse.org/bugs/show_bug.cgi?id=516850
* https://bugs.eclipse.org/bugs/show_bug.cgi?id=516852
* https://bugs.eclipse.org/bugs/show_bug.cgi?id=516853
* https://bugs.eclipse.org/bugs/show_bug.cgi?id=516854

This requires an enormous amount of work and unfortunately, we did not receive enough donations this year to fund the project. **If you want to see this work happen for the Eclipse Platform, [please donate](https://www.eclipse.org/donate/) or [reach out to me](mailto:mikael.barbero@eclipse-foundation.org) if you want to do corporate donations**, or to learn more about corporate donations.

---

Originally published at [mikael-barbero.medium.com](https://mikael-barbero.medium.com/chromium-eclipse-swt-integration-c61f416e97d1)
