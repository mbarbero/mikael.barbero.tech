---
title: "Update on Security improvements at the Eclipse Foundation"
date: 2022-11-24T17:00:00+02:00
tags:
  - security
  - opensource
---

Thanks to [financial support](https://blogs.eclipse.org/post/mike-milinkovich/open-source-security-eclipse-foundation) from the [OpenSSF's Alpha-Omega project](https://openssf.org/community/alpha-omega/), the Eclipse Foundation is glad to have made significant improvements in the last couple of months. Our [previous analysis]({{< ref "/blog/post/eclipsefdn-scorecard-aug2022" >}}) helped us prioritize work area where improvements would be the most significant. Let's see where we are today.

<!--more-->

## Protect the branches from GitHub project

One of the main issue that has been identified by Scorecard during our previous analysis is the lack of branch protection on our repositories at GitHub. Trying to set this up *manually* on all of our 1000+ repositories is not scaling. We need some tooling. We've reviewed the tool on the market that help to [manage GitHub organizations and repositories at scale](https://github.com/todogroup/todogroup.org/blob/main/content/en/guides/management-tools.md#github-management-at-corporate-scale), but none were complying with our requirements in terms of security, workflow, or ease of use. Also, we are a strong proponent of `As Code` approach. We think this principle helps tremendously in being open and transparent. The Eclipse Foundation advocates these two principles as the basis for collaborating and innovating with Open Source.

As such, we've started to work on our [own custom solution](https://gitlab.eclipse.org/eclipsefdn/security/otterdog), based on [an idea](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1531) from [George Adams](https://projects.eclipse.org/user/14362). The project is named **Otterdog** (because 🦦🐶 >> 🐙🐱). The idea is to let an administrator define a default configuration for organizations and repositories, and encode only the difference for specific projects. It's still in its infancy and focus currently on retrieving (at scale) the configuration from GitHub and store the variation from the default configuration.

The project relies heavily on [Jsonnet](https://jsonnet.org) for the configuration as code part, Bash script and the [Github CLI tool](https://cli.github.com) to interact with the REST and GraphQL API, and also with [Puppeteer](https://pptr.dev) for all settings that are not available through Github APIs.

We've implemented *a* [default setting](https://gitlab.eclipse.org/eclipsefdn/security/otterdog/-/blob/main/orgs.libsonnet) in the repository. The settings file for the [Eclipse OpenJ9](https://github.com/eclipse-openj9) organization would look like:

```jsonnet
local orgs = import './orgs.libsonnet';

orgs.newOrg('eclipse-openj9') {
  api+: {
    billing_email: 'webmaster@eclipse.org',
    dependabot_alerts_enabled_for_new_repositories: false,
    dependabot_security_updates_enabled_for_new_repositories: false,
    dependency_graph_enabled_for_new_repositories: false,
  },
  puppeteer+: {
    'settings/discussions'+: {
      discussions_enabled: false,
    },
    'settings/member_privileges'+: {
      members_can_change_repo_visibility: true,
      members_can_delete_repositories: true,
      readers_can_create_discussions: true,
    },
    'settings/packages'+: {
      packages_containers_internal: false,
      packages_containers_public: false,
    },
  },
  repositories+: [
    {
      default_branch: 'master',
      description: "Eclipse OpenJ9: A Java Virtual Machine for OpenJDK that's optimized for small footprint, fast start-up, and high throughput.   Builds on Eclipse OMR (https://github.com/eclipse/omr) and combines with the Extensions for OpenJDK for OpenJ9 repo.",
      branch_protection_rules: [
        { pattern: 'v*-release' },
        { pattern: 'master' },
        { pattern: 'jitaas' },
      ],
      name: 'openj9',
    },
    {
      default_branch: 'openj9',
      description: "Eclipse OpenJ9's clone of the Eclipse OMR (https://github.com/eclipse/omr) project.  PRs should be opened against the upstream OMR project whenever possible.",
      branch_protection_rules: [
        { pattern: 'v*-release' },
        { pattern: 'openj9' },
        { pattern: 'jitaas' },
      ],
      name: 'openj9-omr',
    },
    ...
  ],
}
```

## Apply principle of least privilege to GitHub workflows' tokens

We've been contacted by [StepSecurity](https://www.stepsecurity.io) in order to evaluate their solution that makes it easy to submit PR with fixes for some issues reported by [Scorecard](https://github.com/ossf/scorecard). We've been quite impressed by the ease of use of their solution:

![StepSecurity Dashboard](stepsecurity-dash.jpg)

The dashboard provides a nice overview of the repositories and their scorecard results. Their is also an histogram of the security score for all repositories in the organization which is similar to what we did manually in our [previous analysis]({{< ref "/blog/post/eclipsefdn-scorecard-aug2022" >}}). But the killer feature for this app is the ability to automatically create pull-requests to fixes some of the issues identified by scorecards, e.g.:
* Pin Actions to a full length commit SHA
* Restrict permissions for GITHUB_TOKEN

StepSecurity also recently released a new feature that helps with properly configuring dependabot on repositories.

We have [started to use StepSecurity](https://github.com/eclipse/jkube/pull/1766) on some of our repositories and we are very satisfied of the results. A couple of missing features from the early days have already been implemented: previewing the pull-request before opening it, the ability to customize the PR comment... We will now use this across all our organization and repositories and will have a second run of full analysis to watch how our scorecard results improved.

## SLSA badging program

Projects at Eclipse Foundation can now declare their [SLSA compliance level](https://slsa.dev/spec/v0.1/levels) on their Project Management Infrastructure (PMI) page. Next improvement will help projects with [specifying how to comply with requirements for each level](https://slsa.dev/spec/v0.1/requirements).

![SLSA Compliance Badge Example](temurin-slsa.jpg)

Some projects already have the SLSA compliance process [well underway](https://adoptium.net/docs/secure-software/). Eclipse Temurin (part of Adoptium), for example, just recently completed the work necessary to [reach level 2 SLSA](https://adoptium.net/docs/slsa/) compliance, and is working on achieving level 3.

## Revise vulnerability reporting practices

An [RFC has been published](https://docs.google.com/document/d/1Cv9PIpzVVhKOSLJP1Qc9RTorvhEDkCcuY3F8-Z8jNnQ/edit#) and socialized with the [security team](https://www.eclipse.org/security/) and the [architecture council](https://www.eclipse.org/org/foundation/council.php), proposing several changes and improvements to the vulnerability disclosure processes at the Eclipse Foundation. The RFC also outlines how to leverage tooling at gitlab.eclipse.org and github.com in order for projects to receive vulnerability reports in confidential channels and to work on security fixes under embargo.

[Eclipse Californium](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1705) and [Eclipse Oniro](https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/1602) have started to experiment with this tooling on github.com and gitlab.eclipse.org respectively.

[![StepSecurity Dashboard](rfc.jpg)](https://docs.google.com/document/d/1Cv9PIpzVVhKOSLJP1Qc9RTorvhEDkCcuY3F8-Z8jNnQ/edit#)

## Improve security of marketplace.eclipse.org

The Eclipse Marketplace is a successful service that serves more than 12 millions API requests per month. In an effort to better protect its users, we've [started to enforce the use of HTTPS]({{< ref "/blog/post/enforce-https-eclipse-marketplace" >}}) for all contents linked by the Eclipse Marketplace on October 14th, 2022. The Eclipse Marketplace does not host the content of the provided solutions, it only provides links to them. We are actively working with owners of existing solutions with plain HTTP entries to fix them. Beginning December 15th, non-compliant solutions will be unpublished from the Eclipse Marketplace. This effort can be followed on the [tracking issue](https://gitlab.eclipse.org/eclipsefdn/security/public-tasks/-/issues/1).

## Public presentations

We've been busy presenting why securing the supply chain of open source software is important what we do at the Eclipse Foundation to help our projects do that:

* [EclipseCon 2022](https://www.eclipsecon.org/2022/sessions/open-source-software-supply-chain-security-—-why-does-it-matter): Open Source Software Supply Chain Security — Why does it matter? ([slides](https://www.eclipsecon.org/sites/default/files/slides/2022_10%20—%20EclipseCon%202022%20-%20Open%20Source%20Software%20Supply%20Chain%20Security%20—%20Why%20does%20it%20matters_.pdf), [video recording](https://www.youtube.com/watch?v=UMLfEL9YqEE))
* [Open Source Experience](https://www.opensource-experience.com/en/event/): Open Source Software Supply Chain Security — Why does it matter? (video upcoming)
* [Open CIO Summit](https://www.openciosummit.org/programme-148): Les communautés open source et les enjeux autour du Secure & Green By Design (video upcoming)

## We're hiring!

We've still plenty to do:
* we are conducting code audits with [OSTIF](https://ostif.org) for a couple of our projects,
* we will soon start a campaign to promote the usage of Two-Factors Authentication to our committers,
* leverage our [ORT setup](https://ort-vm1.eclipse.org/) and the SBOM it generates to help our projects publish a SBOM with their releases;
* implement a digital signature infrastructure for all Eclipse Foundation projects (best candidate for that is Sigstore).
* ... and many other things

To do that, we are [growing the team](https://www.eclipse.org/careers/). We currently have 3 openings:

* [Open Source Software Engineer](https://eclipsefoundation.applytojob.com/apply/2IInSY5BF4/Open-Source-Software-Engineer?source=Mikael%20Barbero%20s%20Blog)
* [Java/Go/Rust Software Engineer for Security Tooling](https://eclipsefoundation.applytojob.com/apply/QXUF8CCMMe/JavaGoRust-Software-Engineer-For-Security-Tooling?source=Mikael%20Barbero%20s%20Blog)
* [SecOps Engineer](https://eclipsefoundation.applytojob.com/apply/ua0PGrACt0/SecOps-Engineer?source=Mikael%20Barbero%20s%20Blog)

If you're interested in the topic of Software Supply Chain security and would like to actively participate in securing the 420+ projects developed at the Eclipse Foundation, [you should consider applying](https://www.eclipse.org/careers)!

![Go For It](brett-jordan-MHS7g_q-6mE-unsplash.jpg "Photo by [Brett Jordan](https://unsplash.com/@brett_jordan) on [Unsplash](https://unsplash.com)")


