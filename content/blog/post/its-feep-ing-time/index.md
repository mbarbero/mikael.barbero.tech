---
title: "It’s FEEP-ing time!"
date: 2016-06-02T17:03:00+02:00
tags:
  - Eclipse IDE
  - FEEP
---

FEEP does sound like a bird call, but it stands for *Friend of Eclipse Enhancement Program*. Yes, it is a mouthful. You may wonder what it stand for… Well, let me tell you!

<!--more-->

FEEP is a program launched by the Eclipse Foundation late last year to utilize all the donations made through the Friends of Eclipse program to make significant and meaningful improvements and enhancements to the Eclipse IDE/Platform. Prior to this launch, all donations were being reinvested into the general Eclipse community, but FEEP commits to investing all your donations into improving the Eclipse Platform. In other words, **100% of your donation will fund Eclipse Platform improvements!**

Since the program was first launched in 2015, we’ve already made many concrete improvements and we are planning even more. We have been working closely with representatives from the Eclipse Architecture Council and Project Management Committee (PMC) to identify bugs and problems that are important to community members and that will improve your everyday coding life. Here is what we’ve done so far thanks to your donations:

![FEEP](iMASvD3Y1PGSREUn.png#floatright)

* **Mac install experience**: Eclipse is now packaged as a native OS X application, which means it’s now easier for Mac users to install Eclipse packages!
* **GTK3 support**: GTK3 support has been improved, making the Linux user experience much better.
* **Download and Updates**: p2 metadata is now distributed with a new compression scheme (xz) to be downloaded faster. Processes relying on these downloads (like check for updates and dependency resolution) now requires less waiting time!
* **Autosave of all workbench editors**: As the title implies, all workbench editors are now automatically saved, which prevents the unfortunate loss of work.
* **Dark theme for Windows**: Updates were made to the Eclipse Dark theme to make it easier to work with and more appealing to the user’s eyes.
* **Lower memory consumption**: some memory leaks, especially in the toolbar handling, have been fixed. It means one thing: fewer OutOfMemoryError!

To experience these changes in your workspace you’ll want to install the latest version of Eclipse Neon. [Download Eclipse Neon](https://www.eclipse.org/downloads/) (4.6) now (or use a [developer build](https://www.eclipse.org/downloads/index-developer.php) if you see this message before the release scheduled on June 22nd 2016).

**FEEP is an ongoing program and we’re continually planning new enhancements**. Like any well laid out plan, the upcoming fixes are always subject to change, but as the plan stands today, here are the the next targeted fixes:
* Even faster download of Eclipse updates
* Identify and report non cancelable background tasks easily
* More improvements to the Windows dark theme
* [No more “resource is out of sync with the filesystem” error](https://projects.eclipse.org/development_effort/enable-refresh-using-native-hooks)
* Add the [ability to choose which Java runtime](https://projects.eclipse.org/development_effort/fix-poor-user-experience-jvm-selection-dialog) to be used to start Eclipse.
* [XML Editor improvements](https://projects.eclipse.org/development_effort/wtp-xml-editor-technology-face-lift)

Obviously, **the development depends on the amount of donations received**. The great news is, you can make a difference and help improve the Eclipse Platform! Don’t be shy, every dollar counts and the community will benefit from your support.

We also offer benefits to donors joining our Friends and Best Friends program. Find out [how to receive benefits](https://www.eclipse.org/donate/faq.php).

**Make a difference, please [donate today](https://www.eclipse.org/donate/)!**

---

Originally published at [mikael-barbero.tumblr.com](https://mikael-barbero.tumblr.com/post/145308146515/its-feep-ing-time) and [mikael-barbero.medium.com](https://mikael-barbero.medium.com/its-feep-ing-time-5af3f11ae976).