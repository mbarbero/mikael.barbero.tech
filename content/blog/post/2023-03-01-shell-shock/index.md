---
title: "Shell Hole: How Advanced Prompts are Putting Software Developers at Risk"
date: 2023-03-01T10:00:00+02:00
thumbnail: gabriel-aguirre-2BNag31ZzyY-unsplash.jpg
tags:
  - security
  - shell
  - opensource
---

Advanced shell prompts, such as those provided by theme engines like [oh-my-zsh](https://ohmyz.sh) and [oh-my-posh](https://ohmyposh.dev), have become increasingly popular among software developers due to their convenience, versatility, and customizability. However, the use of plugins that are executed outside of any sandbox and have full access to the developer shell environment, presents significant security risks, especially for Open Source Software developers.

<!--more-->

**Open Source Software (OSS) developers are primary targets for software supply chain attacks** because they often have access to a wide range of sensitive information and powerful tools that can be used to amplify the impact of a successful attack. OSS developers often have access not only to source code, but also access keys and credentials, which can be exfiltrated and used for malicious purposes. By compromising a single OSS developer, attackers can potentially gain access to the sensitive information and powerful tools of many other developers and users. This can enable them to launch more sophisticated and damaging attacks, potentially affecting a large number of individuals, organizations, and even whole industries.

For these reasons, OSS developers are primary targets for software supply chain attacks, and it's crucial for them to be aware of the risks and to take steps to protect themselves and their users. **This includes verifying the authenticity and security of any software they use, keeping software up to date, and being vigilant for signs of compromise**.

![Oh My ZSH!](ohmyzsh.jpg "[Oh My ZSH!](https://ohmyz.sh)")

Shell theme engines and other advanced shell prompts is an example of the tools that have gain in popularity in the last couple of years and does not seem to considered as much as a threat as some others like projects dependencies or IDE plugins. **A compromised shell prompt plugin can steal valuable information and credentials** in several ways:

* **Keylogging**: The plugin can capture keystrokes, including sensitive information such as passwords, credit card numbers, and access tokens.
* **Screenshots**: The plugin can take screenshots of the user's screen, potentially capturing sensitive information displayed on the screen.
* **Data exfiltration**: The plugin can exfiltrate data from the user's system, such as sensitive files, source code, or even access tokens.
* **Remote access**: The plugin can open a remote connection to an attacker-controlled system, allowing the attacker to gain access to the user's system and steal sensitive information.
* **Credentials harvesting**: The plugin can harvest sensitive information such as passwords, access tokens, and private keys from the user's system, such as:
  * *System keychain*: Windows Credential Manager, macOS Keychain, Gnome Keyring or KDE KWallet. If misconfigured, they don't require systematic authorization to read entries.
  * *User's configuration files* which are sometime used to store sensitive information.
  * *Shell history* may contain sensitive information passed as arguments to commands (e.g., `curl -H 'Authorization: Bearer xxxxxx').
  * *Environment variables*: environment variables are sometimes used to share credentials with applications without having to pass explicit parameters.
  * *Browser Cookies* file store can be inspected to steal session tokens.
* **Network reconnaissance**: The plugin can gather information about the user's network, such as IP addresses, hostnames, and open ports, which can be used to launch more targeted attacks.

It's worth noting that these are just a few examples of how a compromised shell prompt plugin can steal valuable information and credentials. The actual methods used by attackers may vary and will depend on the attacker's goals.

To mitigate these risks, it is important for software developers to properly configure and secure their advanced shell prompts. This includes verifying the authenticity and security of any plugins before use, regularly patching and updating systems, and monitoring for suspicious activity. Additionally, it is also important to educate developers on the proper use of advanced shell prompts, and the risks associated with them.

In conclusion, advanced shell prompts like [oh-my-zsh](https://ohmyz.sh) and [oh-my-posh](https://ohmyposh.dev) are powerful tools that can greatly enhance productivity and automation for software developers. However, the use of plugins that are executed outside of any sandbox and have full access to the user shell environment, presents significant security risks. **Software developers are particularly vulnerable to software supply chain attacks, which can have serious consequences for their software development environments**. It is important for organizations to take appropriate measures to secure their command-line interfaces and educate their users on the risks associated with them, especially when it comes to using plugins from untrusted sources.
