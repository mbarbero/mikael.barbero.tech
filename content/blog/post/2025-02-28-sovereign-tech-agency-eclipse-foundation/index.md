---
title: "Strengthening Open Source Security: Eclipse Foundation Selected by the Sovereign Tech Agency for a New Service Agreement"
date: 2025-02-28T09:15:00+02:00
thumbnail: STA-Logo-Default-Color-RGB.png
tags:
  - security
  - eclipse-foundation
  - sovereign-tech-agency
---

We are pleased to announce that the Eclipse Foundation has been selected by the [Sovereign Tech Agency](https://www.sovereign.tech/) for a new service agreement. Through this collaboration, the [Sovereign Tech Fund](https://www.sovereign.tech/programs/fund)—a program of the Sovereign Tech Agency—will invest in the development, improvement, and maintenance of open digital base technologies worldwide, driving significant security enhancements across Eclipse Foundation projects.

## Why This Matters

Open source software is the backbone of countless industries and technologies. At the Eclipse Foundation, we host a diverse range of critical projects, including:

* [**Eclipse IDE**](https://eclipseide.org/) — Powers approximately 10% of developers worldwide and widely used in embedded systems and hardware development.  
* [**Jakarta EE**](https://jakarta.ee/) — Provides enterprise-grade Java APIs for leading application servers, including Payara, WildFly, Apache TomEE, and more.  
* [**Eclipse Jetty**](https://jetty.org/index.html) — A high-performance web server essential for scalable and secure web applications, referenced by over 3,700 artifacts in the Maven Central repository.  
* [**Eclipse Temurin**](https://adoptium.net/fr/temurin/releases/) — Delivers secure, high-quality Java distributions, with over 20 million downloads each month.  
* [**Eclipse GlassFish**](https://glassfish.org/) — Offers cloud-native Jakarta EE implementations.  
* [**Eclipse Software Defined Vehicle**](https://sdv.eclipse.org/) **(SDV)** — Collaborates with major European automotive manufacturers to shape the future of software-defined vehicles.

For more than two decades, Java has remained one of the top five programming languages, underpinning critical systems in finance, healthcare, aerospace, and power grid management. Ensuring robust security and reliability for these foundational tools is paramount. Our engagement with the Sovereign Tech Agency will further strengthen the security and long-term sustainability of these essential open source technologies.

## What Is Being Commissioned?

The Sovereign Tech Fund is funding two key initiatives to enhance security, transparency, and vulnerability management across the Eclipse Foundation ecosystem:

### 1\. **Software Bill of Materials (SBOM) Generation**

* **Integration into Build Pipelines** – SBOM generation will be embedded directly into the build processes of Eclipse Foundation projects.  
* **Central SBOM Registry** – A unified repository will provide clear insights into components and dependencies across our portfolio.  
* **SBOM Tools Evaluation** – We will assess and select the most effective tools for various project categories, ensuring compatibility with diverse technology stacks.  
* **Eclipse IDE Support** – SBOM generation capabilities will be developed for Eclipse IDE products, enabling developers to effortlessly produce SBOMs for custom IDEs and products developed on top of the Eclipse Platform.

### 2\. **Vulnerability Management Enhancement**

* **Continuous Vulnerability Monitoring** – We will implement ongoing monitoring solutions to rapidly detect newly discovered vulnerabilities in project dependencies, even post-release.  
* **Developer Education and Best Practices** – Training and resources will empower maintainers and contributors to triage and remediate security issues swiftly, strengthening our overall ecosystem.  
* **New Tooling and Automation** – Investments in advanced scanners and management platforms will streamline vulnerability detection, reporting, and resolution.

These initiatives will reinforce our supply chain security, enhance transparency, and ensure that Eclipse Foundation’s open source projects remain reliable and resilient for the millions of developers who depend on them.

## Looking Ahead

Our partnership with the Sovereign Tech Agency underscores our commitment to open source security and quality. This service agreement further solidifies the Eclipse Foundation’s leadership in delivering critical software technologies worldwide. Ultimately, this collaboration will benefit organisations of all sizes—from large enterprises to individual contributors—who rely on open source software to power their infrastructure, operations, and innovation.

Stay tuned for updates as we make progress on these initiatives. We look forward to sharing new features, best practices, and milestones that will help secure the future of open source technology for everyone.

---

![Sovereign Tech Agency](STA-Logo-Default-Color-RGB.png)

*For additional details about this collaboration, visit the Sovereign Tech Fund’s technology page in [English](https://www.sovereign.tech/tech/eclipse-foundation) and [German](https://www.sovereign.tech/de/tech/eclipse-foundation).*

*For more information about the work of the Sovereign Tech Fund—an initiative of the Sovereign Tech Agency—please visit their [official website](https://www.sovereign.tech/programs/fund).*
