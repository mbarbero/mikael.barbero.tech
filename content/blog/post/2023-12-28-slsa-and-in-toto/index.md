---
title: "Understanding Software Provenance Attestation: The Roles of SLSA and in-toto"
date: 2023-12-28T16:00:00+02:00
thumbnail: agence-olloweb-d9ILr-dbEdg-unsplash-small.jpg
tags:
  - security
  - provenance
  - slsa
  - opensource
---

A [software provenance attestation](https://mikael.barbero.tech/blog/post/2023-12-26-understanding-software-provenance/) is a signed document that associates metadata with an artifact, encompassing details like the artifact's origin, build steps, and dependencies. This information is critical for verifying the artifact's authenticity and integrity. Featuring a cryptographic signature, provenance attestation ensures the document remains unaltered, playing a vital role in mitigating supply chain attacks. By scrutinizing the provenance of binaries, users can thwart the execution of malicious code on their systems. Let's delve into some concrete examples:

1. **Case of Compromised Package Manager**: Imagine a scenario where an attacker gains access to a developer's package manager account for a widely-used open-source software library. The attacker injects malicious code, builds, and publishes an updated library version. As users update the library, the malicious code spreads. With provenance attestation, users would have noticed discrepancies in the attestation, revealing the code did not originate from the official source. This proactive measure could have averted incidents like the [UAParser.js library hijacking](https://therecord.media/malware-found-in-npm-package-with-millions-of-weekly-downloads) in 2021.

2. **Software Supplier Compromise Example**: Consider an attacker targeting a software supplier and tampering with their product. When customers use this compromised product, it could lead to further software breaches or data exfiltration. The [CodeCov attack](https://blog.gitguardian.com/codecov-supply-chain-breach/) serves as a pertinent example. Here, a malicious `bash uploader` artifact was uploaded to CodeCov's repository. Unknowingly downloaded and used this artifact by users resulted in stolen Git credentials and amplified the supply chain attack. Provenance attestation would have enabled users to detect the anomaly and prevent the execution of the malicious code.

## Exploring SLSA

[SLSA (Supply chain Levels for Software Artifacts)](https://slsa.dev) is a framework that enhances the security of software supply chains by defining levels for software projects to aspire to. It emphasizes the generation, distribution, and verification of provenance information.

The framework currently offers three *Levels* under the *Build* track in its 1.0 version. However, it's beneficial to revisit the initial v0.1 draft for a broader understanding of SLSA's scope and future direction.

![Supply Chain Threats](supply-chain-threats.svg)

SLSA aims to shield against tampering at various stages of the software supply chain. The initial levels presented a progressive approach to mitigating these [threats](https://slsa.dev/spec/v1.0/threats). The higher the level, the more robust the supply chain security. The requirements for each level were comprehensive and provided a clear progression path.

![SLSA v0.1 Requirements](slsa-v0.1-requirements.png)

The 1.0 version narrowed its focus to the Build and Provenance requirements. The introduction of "tracks" allows for future expansion, with the newly created Build track being the sole focus in this version. Each track's levels will eventually measure different aspects of supply chain security.

SLSA 1.0 refined the division of requirements between the project and the build platform. Achieving a higher Level in the Build Track implies greater trust in the Provenance information, thereby offering better protection against threats. The project requirements in the build track are now more straightforward, largely depending on the build platform's ability to produce provenance that meets the level's criteria.

![SLSA v1.0 Build Track Requirements](slsa-v1.0-build-track-requirements.png)

## Introducing in-toto

[in-toto](https://in-toto.io) is a framework centered around software attestations, focusing on the generation and verification of metadata of the software supply chain. Developers and stakeholders generate metadata reflecting actions like coding and testing. The project owner creates a layout, a critical document that defines the supply chain's expected steps and corresponding metadata.

Key components of in-toto include the layout (a JSON document outlining expected the supply chain), functionaries (individuals or automated processes executing steps), and inspections (operations that need to be performed on the final product at the time of verification).

While similar to SLSA, in-toto offers broader attestation capabilities than just about provenance information and without specific guidance against a threat model, as seen in SLSA. It provides a standard format for outlining supply chain expectations and realities.

## SLSA and in-toto: hand in hand

Given in-toto's role in software attestation and SLSA's focus on trustable provenance, their combination is logical. SLSA recommends (but does not mandate) the use of the in-toto format for provenance information, defining the necessary information for each Build Level as a [custom in-toto predicate type](https://slsa.dev/spec/v1.0/provenance#schema).

This Provenance predicate aligns with in-toto's framework: it models the supply chain as a series of steps, with each step generating attestations. These attestations are then verified against a supply chain layout, a signed metadata document defining the expected steps and their outcomes.

In-toto's ability to define use-case-specific predicates complements SLSA by providing a flexible means to capture a wide array of contextual supply chain information. This includes code reviews, test results, and runtime traces, all of which can be tailored to the specific needs of a project. 

By integrating SLSA Provenance with in-toto attestations, software supply chains can achieve a comprehensive verification process. This integration allows for detailed tracking and verification of each step in the supply chain, from code development to the final build, ensuring that all components meet the specified security and integrity standards.

In practice, this means that when a build service performs a build, it not only records the SLSA Provenance of the resulting artifact but also integrates this with the broader set of in-toto attestations, encompassing various aspects of the build process. These attestations, when combined, offer a more detailed and trustworthy view of the software's development and build process, enhancing the overall security posture.

## Conclusion

Software provenance attestation is crucial for mitigating a multitude of security threats. Frameworks like SLSA and in-toto play a significant role in enabling these attestations, ensuring the integrity and security of software supply chains. In a forthcoming blog post, we'll explore in detail the process of creating SLSA provenance attestations specifically for Java/Maven projects. This deep dive will provide valuable insights and practical steps for developers looking to enhance the security of their Java applications. Keep an eye out for this upcoming post for a comprehensive guide on implementing these security measures.