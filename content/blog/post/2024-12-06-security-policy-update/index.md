---
title: "Introducing the Updated Eclipse Foundation Security Policy"
date: 2024-12-06T10:00:00+02:00
thumbnail: thumbnail.jpg
tags:
  - security
  - policy
  - eclipse-foundation
---

On November 20, 2024, the Board of Director of the Eclipse Foundation approved version 1.2 of its Security Policy. This update brings significant enhancements aimed at improving the management, resolution, and disclosure of vulnerabilities within the Eclipse community. Here's a rundown of the key changes and what they mean for Eclipse projects and users.

<!--more-->

## Renaming to "Security Policy"

One of the first noticeable changes is the renaming of the policy from the **"Eclipse Foundation Vulnerability Reporting Policy"** to the **"Eclipse Foundation Security Policy."** This change reflects a broader scope, encompassing not just the reporting but also the overall management of security within the Foundation's projects.

## Introduction of the Project's Security Team

The updated policy introduces the concept of a **"Project's Security Team."** This team is responsible for coordinating the resolution of vulnerabilities within their specific project. By default, all committers of a project are included in its Security Team, but projects have the flexibility to establish specific criteria for team membership.

### Relationship with the Eclipse Foundation Security Team

The **Eclipse Foundation Security Team (EF Security Team)** plays a supportive role, providing guidance and assistance to Project Security Teams. While the EF Security Team oversees security coordination across the Foundation, the Project's Security Teams are on the front lines, handling the specific vulnerabilities within their projects.

## Clarified Roles and Responsibilities

The policy clarifies the roles and responsibilities of both the EF Security Team and the Project's Security Teams:

- **Discussion:** Initial discussions about potential vulnerabilities occur privately between the reporter, the Project's Security Team, and the EF Security Team. Once confirmed, these discussions are moved to an Eclipse Foundation-supported issue tracker to proceed with mitigation.

- **Resolution:** All vulnerabilities **must be resolved.** A vulnerability is considered resolved when a fix or workaround is available, or when it's determined that a fix is not possible or desirable (in which case, a risk analysis must be provided).

- **Distribution:** Once resolved, updated software containing the fix should be made available to the community through regular or service releases.

- **Disclosure:** It's strongly recommended that public disclosure occurs immediately after a bugfix release is available. In all cases, users and administrators must be informed of any vulnerabilities to assess risks and take appropriate actions.

## Emphasis on Timely Resolution and Disclosure

The updated policy sets expectations for timely handling of vulnerabilities:

- Projects are expected to resolve vulnerabilities within **no more than three months.** Extensions may be granted by the Project Leadership Chain in consultation with the EF Security Team if necessary.

- Resolved vulnerabilities should be disclosed immediately after the release containing the fixes is available, ensuring transparency and enabling users to protect their systems promptly.

## Enhanced Collaboration and Communication

The policy encourages close collaboration between the Project's Security Teams and the EF Security Team. Projects are advised to communicate their preferred methods of contact and keep the EF Security Team updated on any changes. Additionally, the EMO (Eclipse Management Organization) will assist in setting up confidential channels for discussing undisclosed vulnerabilities when needed.

## Conclusion

The updates in version 1.2 of the Eclipse Foundation Security Policy strengthen the security posture of the Eclipse community by clarifying processes and responsibilities. These changes aim to ensure that vulnerabilities are managed effectively, resolutions are timely, and communications are transparent.

We encourage all Eclipse project teams and users to familiarize themselves with the new policy to understand how it affects their roles and responsibilities within the community.

For more detailed information, please refer to the [Eclipse Foundation Security Policy](https://www.eclipse.org/security/policy/).