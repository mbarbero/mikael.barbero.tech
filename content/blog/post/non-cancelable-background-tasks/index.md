---
title: "Identify non cancelable background tasks in Eclipse"
date: 2016-05-09T08:31:00+02:00
tags:
  - Eclipse IDE
---

Two weeks ago I was in Paris for [Devoxx France 2016](http://www.devoxx.fr) where I’ve presented [what’s new](http://www.slideshare.net/mikaelbarbero/the-eclipse-ide-the-force-awakens-devoxx-france-2016) in the upcoming Eclipse release (aka Neon — to be released in June). During the talk, I’ve been asked if Eclipse will eventually cancel a background task ([a job in the Eclipse terminology](http://help.eclipse.org/mars/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Fguide%2Fruntime_jobs.htm)) when it is asked for. Who never fulminate against a progress bar stating that cancel has been requested and that the task does not finish quickly?

<!--more-->

![Building worskpace...](images/JHwjMUOUGjgGkmJ2.png)

I explained that Eclipse (the platform) can’t do much about the situation. The contract of the job API is clear: when a user asks for a cancelation, the job is informed about this request through its [progress monitor](http://help.eclipse.org/mars/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fcore%2Fruntime%2FIProgressMonitor.html). It is up to the job implementations to check for the cancelation requests. They should do it on a regular basis, but there is no way to force them to do so. If a job does not check for it and if it does not exit quickly after a cancelation request, a job will stay in the *Cancel Requested* state until it stops. You may wonder: *why does the platform not have a kill switch for jobs? Why has the API contract never been changed to introduce a forced cancelation of jobs?* Well, it is rather simple: jobs are based on Java threads and [it is inherently unsafe to stop a thread from the outside](https://docs.oracle.com/javase/8/docs/technotes/guides/concurrency/threadPrimitiveDeprecation.html).

So, are we doomed to live with non cancelable jobs? Is there a way to improve the user experience? The only way is to help plug-ins developers to better use the job API and make them check for cancelation requests more often. This is a collective work. As a user, you could give this feedback to the projects you use. Unfortunately, this is not an easy task. When you see that the project failed to check for cancelation, it is usually when you actually try to cancel a task and that it does not respond. This leads to two issues:

* you are already living a bad user experience (problem 1),
* you don’t have a lot of information to provide to the project about the actual task which is not cancelable (problem 2).

After the conference, I was decided to improve the situation. First, I searched for bugs related to the cancelability of jobs. I found [one reported by Alex Blewitt](https://bugs.eclipse.org/bugs/show_bug.cgi?id=470175). His idea is to let users know the last time a job has checked for a cancelation request. I've pushed the reasoning a bit further and implemented a job monitoring system. The idea is similar to the [UI responsiveness monitoring](http://help.eclipse.org/mars/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Freference%2Fref-uiresponsiveness_preference_page.htm&cp=0_4_1_51) that has been implemented last year: when a job does not check for cancelation request often enough (or even worst, when it does not do any check), an error or a warning is logged depending on thresholds:

![Error logs](images/L2G6h6m5Odh0jPJf.png)

The functionality is de-activable and the thresholds are configurable in a preference page. As the system is constantly monitoring all jobs, it lets users and developers identify tasks that don’t reply to cancelation requests fast enough, without actually living the bad user experience by canceling them. Problem 1 solved!

![Preferences](images/Zz71NWS88R7TPBi0.png)

As the amount of code executed by a job can be huge, identifying where cancelation checks should be added can be cumbersome. To help with that, the system also log stack samples before and after the longest periods without cancelation check. It helps projects to identify where cancelation request checks should be added (problem 2 solved).

![Event details](images/b1Hz3Tk-y_ZGFxf8.png)

For instance on the screenshot above, you can see that the method *ReferenceAnalyzer.analyze* in PDE has been the last one to check for cancelation before a long gap. On the the screenshot below, the same method is the first one to check for cancelation after the long gap. It is clear that the PDE API Tooling code would benefit from checking the cancelation requests more often. I’ve filled [bug 493198](https://bugs.eclipse.org/bugs/show_bug.cgi?id=493198) for this issue.

![Event details](images/Qy7WR_0VIG3kSZqx.png)

The job cancelability monitoring system really becomes interesting when combined with the [Automated Error Reporting (AERI)](https://www.infoq.com/news/2015/03/eclipse-mars-reporting). If the error reporting is activated, every time a job does not check cancelation requests often enough, a new report will be made and the project to be blamed will be informed about the issue. Hopefully, it will help to improve job implementations and get rid of the jobs staying in the *Cancel Requested* state for long periods.

This system is not merged yet and is still waiting for approval. It consists of two patchsets:

* a core one that [adds the monitoring to the job infrastructure](https://git.eclipse.org/r/#/c/71594/). It has been merged but then reverted as we currently are in the feature freeze time period for the next Eclipse release. Late addition of a new feature requires special approval, which it will hopefully get. If you’re interested in this feature, feel free to add a comment on [bug 470175](https://bugs.eclipse.org/bugs/show_bug.cgi?id=470175) to say so.
* a UI one that [adds the preference page](https://git.eclipse.org/r/#/c/71623/) to the Eclipse workbench.

---

Originally published at [mikael-barbero.tumblr.com](https://mikael-barbero.tumblr.com/post/144085408850/non-cancelable-background-tasks) and [mikael-barbero.medium.com](https://mikael-barbero.medium.com/identify-non-cancelable-background-tasks-in-eclipse-2bc9f36c82f1).