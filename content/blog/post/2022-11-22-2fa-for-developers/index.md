---
title: "Open Source Software Supply Chain Security starts with developers"
date: 2022-11-22T17:00:00+02:00
tags:
  - opensource
  - security
  - 2fa
---

Open Source Software Supply Chain is at risk: threat actors are shifting target to amplify the blast radius of their attacks and as such increasing their return on investment. Over the past 3 years, we've witnessed an astonishing [742% average annual increase](https://www.sonatype.com/state-of-the-software-supply-chain/introduction) in Software Supply Chain attacks. To make it worse, the attack surface of the supply chain is wide. Covering it all requires a deep scrutinity of many factors. However, there is a simple thing, easy, and free, that every open source developer should do right now: activate multi factor authentication (also known as two factor authentication) on all development related accounts.

<!--more-->

At the origin of any software, there is code written by developers. In the case of open source software, this code published on a publicly accessible code repository. The permission to write to these repositories is protected by the authentication system of the hosting platforms. By default, for most of them, it means basic password-based authentication. Simple and convenient, **password-based authentication is also very fragile**. It can be attacked by social engineering, credential theft or leakage, and many other low cost attacks to get access to developer accounts.

Compromised accounts can then be used to push malicious changes to code they have access to. The risk is of course for the developer associated with the compromised account, but all downstream users of the affected code are also at risk. It can be code from other developers that depends on the infected code, but also users of products that may now run malicious code that will be used for more credentials theft and reach an even larger number of targets.

As such, it is the **responsibility for every Open Source developer** to diligently protect their accounts. The very first line of defense is to move beyond basic password-based authentication and to activate two factor authentication (2FA). This is no silver bullet, and there are [ways to compromise 2FA-protected accounts](https://theconversation.com/can-i-still-be-hacked-with-2fa-enabled-144682). But it is the most cost-effective solution to protect an account: attacking an account protected by 2FA is several orders of magnitude more complex than targeting an account using basic password-based authentication.

If you're an Open Source software developer, I encourage you to **activate 2FA today, on all platforms where it's available**. See below links to documentation how to activate 2FA for code under the stewarship of the Eclipse Foundation:

* [For projects hosted at GitHub](https://docs.github.com/en/authentication/securing-your-account-with-two-factor-authentication-2fa/configuring-two-factor-authentication)
* [For projects hosted at gitlab.eclipse.org](https://gitlab.eclipse.org/help/user/profile/account/two_factor_authentication.md)

If you are willing to make 2FA mandatory for all committers on your project, feel free to [open a ticket at our help desk](https://gitlab.eclipse.org/groups/eclipsefdn/-/issues), and we will work with you to make it happen.

---

See also: [Software security starts with the developer: Securing developer accounts with 2FA](https://github.blog/2022-05-04-software-security-starts-with-the-developer-securing-developer-accounts-with-2fa/) from https://github.blog.