---
title: "Eclipse Foundation Embraces Sigstore"
date: 2023-12-23T12:00:00+02:00
thumbnail: sigstore.png
tags:
  - opensource
  - security
  - sigstore
  - digital signature
---

As part of our ongoing commitment to fortifying the security of our software development processes, we're excited to announce a significant enhancement for all Eclipse Foundation projects utilizing our Jenkins infrastructure. This advancement comes with the integration of [Sigstore](https://www.sigstore.dev), a cutting-edge solution designed to bolster the security and integrity of software supply chains. By exploring the integration of Sigstore within the Eclipse Foundation's Jenkins setup, this article sets out to demonstrate how this advancement is reshaping secure software development and deployment for Eclipse Foundation projects.

<!--more-->

### What is Sigstore?

[Sigstore](https://www.sigstore.dev) represents a shift in the way we secure software artifacts. This open-source tool offers a transparent and secure method for both signing and verifying software artifacts, including binaries and container images. It's designed to make digital signing simpler for developers by eliminating the complex management of keys. This allows users to confidently verify the artifacts' origins and integrity. At its core, Sigstore's "keyless" signing associates a signature with the signer's identity, rather than a fixed set of keys.

The process begins when a developer obtains an identity token from an identity provider and creates a temporary public/private key pair in memory. This token, along with the public key, is sent to Sigstore’s certificate authority, which verifies the information against the token issuer. If the identity provider is recognized and the token is valid, Sigstore’s authority issues a short-lived certificate that binds the public key to the developer's identity. This binding is crucial as it securely attaches the identity of the signer to the artifact being signed, ensuring traceability and accountability.

![sigstore](sigstore_text.png)

During the signing phase, a transparency log entry is created. This entry is part of a public, tamper-evident ledger that records the artifact's hash, the public key used, and the signature, all with a timestamp to validate the software's integrity and origin at the time of signing. Once the signing is complete, the private key is discarded, and the short-lived certificate soon expires. 

The trust in the verification process comes from this transparency log, not from the signer's ability to safeguard a private key. Users can validate the logged details against the artifact to confirm its integrity and origin. This verification can occur online, with real-time access to the transparency log for the most up-to-date information. For environments where the transparency log is not accessible, such as air-gapped systems, offline verification is also possible. In these scenarios, the signed artifacts should be accompanied by the certificate and public key, allowing verification against these components without needing access to the transparency log. This method relies on the trust established by the Sigstore-issued certificate, ensuring the authenticity of the artifact as confirmed by a trusted CA.

This methodology goes beyond improving convenience; it serves as a strategic defense against a range of cyber threats, particularly those targeting software supply chains. By eliminating the need for developers to manage long-lived keys and by providing a transparent log of signed artifacts, Sigstore mitigates risks like code tampering (e.g., when used to sign commits) and unauthorized access, which are prevalent in supply chain attacks.

### Using Sigstore on Eclipse Foundation's Jenkins instances

The Eclipse Foundation has [recently become](https://github.com/sigstore/fulcio/pull/1472) a recognized identity provider for Sigstore's certificate authority. This development is a game-changer for projects within the Foundation for several reasons:

1. **Managed Identity Verification**: With this status, the Eclipse Foundation can issue tokens for projects' bot accounts. These tokens are recognized and verified by Sigstore, which then issues certificates based on the Eclipse Foundation's managed identity. This process ensures a trusted link between the artifact and the Foundation, further bolstering trust and security.

2. **Streamlined Artifact Signing**: Initially focusing on bot accounts, this setup is tailored for automated processes, like those running on Jenkins instances. Projects can seamlessly sign artifacts during the build and release process, integrating security into the CI/CD pipeline without added complexity.

3. **Extended Trust and Compliance**: Having the Eclipse Foundation as a recognized identity provider means that artifacts signed through this process are backed by a trusted entity, meeting higher standards of security.

It's worth noting that Sigstore can be used by all of Eclipse Foundation projects hosted on GitHub and using GitHub Actions, as detailed in [GitHub's blog post](https://github.blog/2022-10-25-why-were-excited-about-the-sigstore-general-availability/). For Eclipse Foundation projects that utilize both Jenkins and GitHub, this creates a cohesive and secure workflow for signing artifacts across platforms.

### Implementing Sigstore in Your Jenkins Workflow

If you want to start signing artifacts with Sigstore's keyless process in your Jenkins workflow, it's very easy:

* The very first step is to open a help desk ticket to ask us to allow our identity provider to issue tokens that would be verifiable by Sigstore. We also configure your Jenkins instance with some new credentials.
* Adapt the workflow below to your use case and profit.

```groovy
pipeline {
  agent any

  stages {
    stage('Prepare') {
      steps {
        sh '''
            echo "Hello World" > README
            curl -sSL -o cosign https://github.com/sigstore/cosign/releases/latest/download/cosign-linux-amd64
            chmod u+x cosign
        '''
      }
    }
    stage('Sign') {
      steps {
        withCredentials([usernamePassword(credentialsId: 'cbi-dev-sigstore', passwordVariable: '_BOT__PASSWORD', usernameVariable: '_BOT__USERNAME')]) {
            sh '''
                IDP_DATA=$(mktemp)
                OID_TOKEN=$(mktemp)
                chmod 600 "${IDP_DATA}" "${OID_TOKEN}"
                trap 'rm -vf "${IDP_DATA}" "${OID_TOKEN}"' EXIT

                cat <<EOF > "${IDP_DATA}"
username=${_BOT__USERNAME}
&password=${_BOT__PASSWORD}
&grant_type=password
&client_id=sigstore
EOF

                curl -sSL -X POST \
                  --url https://auth.eclipse.org/auth/realms/sigstore/protocol/openid-connect/token \
                  --header "Content-Type: application/x-www-form-urlencoded" \
                  --data @"${IDP_DATA}" \
                  | jq -r ".access_token" \
                  | head -c -1 > "${OID_TOKEN}"
                
                ./cosign sign-blob README -y --bundle README.bundle --oidc-issuer=https://auth.eclipse.org/auth/realms/sigstore --identity-token="${OID_TOKEN}"
            '''
        }
        sh '''
            ./cosign verify-blob README --bundle README.bundle --certificate-oidc-issuer=https://auth.eclipse.org/auth/realms/sigstore --certificate-identity=cbi-dev@eclipse.org
        '''
      }
    }
  }
}
```

During the `Prepare` phase, we just download the `cosign` tool, which is a CLI client to sigstore. We could also go the hard way and only communicate with Sigstore via its REST API with curl, but `cosign` make is much simpler. 

During the `Sign` phase, we start by retrieving the project's bot credentials and use `curl` to retrieve a token from the Eclipse Foundation identity provider. We aim at making this phase transparent to projects in the future and create the token automatically on each workflow startup, à la `GITHUB_TOKEN`. We then pass this token to the cosign tool to sign the `README` file. 

Note that we save the file what `cosign` call a `--bundle`. This bundle is just an aggregate of the signature and the certificate of the signature. This avoids having to distribute 2 files along with the signed artifacts, simplifying the transfer and the verification.

At the end of the signing process, the `cosign` tool prints the index of the transparency log entry that has been created:

```
tlog entry created with index: 58260299
```

One can then check the information relative to this operation by going on the transparency log web interface at https://search.sigstore.dev/?logIndex=58260299.

![rekor](rekor.png)

Finally, for testing purpose, we verify the signature during the `Verify` phase. We reuse the bundle we just introduced, and ask the `cosign` tool to verify that the file has been signed with a certificate for the identity `cbi-dev@eclipse.org` as issued by the identity provider `https://auth.eclipse.org/auth/realms/sigstore`. 

```
+ ./cosign verify-blob README --bundle README.bundle --certificate-oidc-issuer=https://auth.eclipse.org/auth/realms/sigstore --certificate-identity=cbi-dev@eclipse.org
Verified OK
```

### Conclusion

We encourage all project teams within the Eclipse Foundation to adopt this new capability. The integration is straightforward and offers significant benefits in securing your software artifacts. By doing so, you'll be taking a proactive stance in securing your projects and contributing to a safer software supply chain.

In conclusion, the adoption of Sigstore within our Jenkins infrastructure is more than just a technical update; it's a commitment to the security and integrity of the Eclipse Foundation projets. We look forward to seeing its positive impact on our community.

We welcome [feedback and questions](https://accounts.eclipse.org/mailing-list/eclipse-cyber-risk-initiative) from the Eclipse Foundation community on this journey together towards a more secure software future.

---

This work was made possible thanks to the funding the Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project. 