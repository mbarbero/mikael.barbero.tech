---
title: "Eclipse Jetty Security Audit Has Been Completed"
date: 2023-10-18T17:00:00+02:00
thumbnail: jetty-avatar-256.png
tags:
  - security
  - audit
  - opensource
---

We’re proud to share that the Eclipse Foundation has completed the security audit for [Eclipse Jetty](https://eclipse.dev/jetty/), one of the world’s most widely deployed web server and servlet containers. All users are encouraged to [upgrade to versions](https://eclipse.dev/jetty/download.php) containing changes addressing all conclusions of the audit: Eclipse Jetty `12.0.0`, `11.0.16`, `10.0.16`, and `9.4.53`.

<!--more-->

Web/application servers and client libraries like ones included in Jetty need to support multiple protocols, such as HTTP/1, HTTP/2, HTTP/3, and various Jakarta EE standards. They handle data that could potentially come from malicious sources. Achieving this in a proper and secure manner can be quite challenging, and even experienced developers may inadvertently make errors.

Additionally, such Jetty servers and clients are integrated with custom application code that handles data. The complete solutions are complex, and improving the security of each building block is crucial. This made Eclipse Jetty a good candidate for a security audit.

The audit provided a set of general recommendations on the direction of Jetty’s architecture, and revealed some issues in the code base that were unknown to the development team. It has also led to two CVEs: [CVE-2023-36479](https://nvd.nist.gov/vuln/detail/CVE-2023-36479) and [CVE-2023-36478](https://nvd.nist.gov/vuln/detail/CVE-2023-36478), along with a list of bug fixes for the initial release of Jetty 12.0.0.

[![Full Report](jetty-audit-report-cover.jpg "[Full Report](https://github.com/trailofbits/publications/blob/master/reviews/2023-03-eclipse-jetty-securityreview.pdf)")](https://github.com/trailofbits/publications/blob/master/reviews/2023-03-eclipse-jetty-securityreview.pdf)

Audits like these improve the security of the whole web services ecosystem for Java applications, both in the short term through fixes, and in the long term by showing potential risks and possible development directions.

While Jetty has been hosted at the Eclipse Foundation since 2009, the project’s origins go back to 1995. Jetty is used extensively by millions of developers and in production environments around the world. Its small footprint, high performance, and scalability have made the server an appealing choice among enterprise application developers using a variety of Java, Scala, Kotlin, and other JVM-based languages. Jetty can be found in products and projects such as Apache Hadoop, Apache Maven, Google App Engine, Dropwizard, Spring Boot, the Javalin project, Zimbra, and the [Eclipse IDE](https://eclipseide.org/). 

This was our third open source project security audit, and was completed by [Trail of Bits](https://www.trailofbits.com/). Like our previous two audits, this initiative was done in collaboration with the [Open Source Technology Improvement Fund](https://ostif.org/) (OSTIF) and was made possible thanks to the funding the Eclipse Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project. 

## Get Involved

* Download [Eclipse Jetty](https://eclipse.dev/jetty/), and learn how you can contribute to the project.
* Learn more about the [Eclipse Cyber Risk Initiative](https://www.eclipse.org/org/workinggroups/eclipse-cyber-risk-concept.php), and how your organization can join the effort to strengthen the open source supply chain. Please subscribe to the [ECRI mailing list](https://accounts.eclipse.org/mailing-list/eclipse-cyber-risk-initiative) to join the initiative, or to follow its progress.
