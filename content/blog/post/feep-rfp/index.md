---
title: "RFPs for new Friends of Eclipse Enhancement Program (FEEP) projects posted"
date: 2016-12-09T17:11:38+02:00
tags:
  - Eclipse IDE
  - FEEP
---

The Eclipse Foundation is actively seeking bids on **10 new [FEEP](https://eclipse.org/contribute/dev_program.php) Development Efforts**, along with 5 outstanding Development Efforts that have not yet been bid. The objective is to have this work completed in the 1st quarter of 2017.

<!--more-->

Please see the [list of development efforts](https://projects.eclipse.org/development-efforts), and **submit your bids before December 23, 2016**. See the [FAQ for the bidding process](https://eclipse.org/contribute/dev_program/faq.php).

This list has been crafted based on the feedbacks of the Eclipse PMC and the Eclipse Architecture council members. It also gathered some of the most wanted bugs on the bugzilla (i.e. the bugs with the highest number of votes).

As a reminder, the [FEEP program](https://eclipse.org/contribute/dev_program.php) uses the funds donated to the Eclipse Foundation to make direct improvements and enhancements to the Eclipse IDE/Platform.

---

Originally published at [mikael-barbero.tumblr.com](https://mikael-barbero.tumblr.com/post/154241861340/rfps-for-new-friends-of-eclipse-enhancement) and [mikael-barbero.medium.com](https://mikael-barbero.medium.com/rfps-for-new-friends-of-eclipse-enhancement-program-feep-projects-posted-e957f853cbc9)