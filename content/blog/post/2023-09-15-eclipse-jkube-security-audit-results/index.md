---
title: "Eclipse Foundation Publishes Results of Eclipse JKube Security Audit"
date: 2023-09-15T16:00:00+02:00
thumbnail: JKube-Logo-final-horizontal-color.png
tags:
  - security
  - audit
  - opensource
---

Today, the Eclipse Foundation released the results of our security audit for [Eclipse JKube](https://eclipse.dev/jkube/), a collection of tools for building Java applications that can be deployed to a cloud environment. Findings from the audit have been addressed in the [1.13 release](https://blog.marcnuri.com/eclipse-jkube-1-13#kubernetes-security-hardening) leading to a new feature.

<!--more-->

This audit included a documentation review, static code review and manual review, which uncovered two findings that have been addressed by the JKube team through container configuration improvements. 

[Eclipse JKube](https://eclipse.dev/jkube/) originated in 2020 as the successor to the Fabric8 Maven plug-in, which was deprecated the same year. Consisting of plug-ins and components for building container images using Docker, Java image builder (Jib), or Source-to-Image (S2I) build strategies, JKube makes it easy to transition Java applications to the cloud by deploying them in Kubernetes.

By default, JKube’s Kubernetes deployment artifacts did not have many of Kubernetes’ security features enabled. To address the audit’s findings and improve the overall security of JKube’s generated Kubernetes resources, the JKube team introduced a Security Hardening profile in the project’s [1.13 release](https://blog.marcnuri.com/eclipse-jkube-1-13#kubernetes-security-hardening). The profile disables the auto-mounting of the service account token, prevents containers from running in privileged mode, and ensures containers do not allow privilege escalation. Check out the [full report](https://github.com/trailofbits/publications/blob/master/reviews/2023-05-eclipse-jkube-securityreview.pdf) for more information.

[![Full Report](report_front.png "[Full Report](https://github.com/trailofbits/publications/blob/master/reviews/2023-05-eclipse-jkube-securityreview.pdf)")](https://github.com/trailofbits/publications/blob/master/reviews/2023-05-eclipse-jkube-securityreview.pdf)

This was our second open source project security audit, and was completed by [Trail of Bits](https://www.trailofbits.com/). Like with the [Equinox p2 security audit](https://mikael.barbero.tech/blog/post/2023-07-12-equinox-p2-security-audit-results/), this initiative was done in partnership with the [Open Source Technology Improvement Fund](https://ostif.org/) (OSTIF) and was made possible due to the funding the Eclipse Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project. 

Impartial security audits like this play an important role in ​​securing the open source software supply chain. Developers looking to containerize Java applications with JKube can now be confident in the security of their applications. 

Keep an eye on our blog for more security audit announcements in the future. 

## Get Involved

* Get started with [Eclipse JKube](https://eclipse.dev/jkube/), and learn how you can contribute to the project.
* Learn more about the upcoming [Eclipse Cyber Risk Initiative](https://www.eclipse.org/org/workinggroups/eclipse-cyber-risk-concept.php), and how your organization can join the effort to strengthen the open source supply chain. Please subscribe to the [ECRI mailing list](https://accounts.eclipse.org/mailing-list/eclipse-cyber-risk-initiative) to join the initiative, or to follow its progress.
