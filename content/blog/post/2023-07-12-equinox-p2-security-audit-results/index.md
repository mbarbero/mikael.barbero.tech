---
title: "Eclipse Foundation Publishes Results of Equinox p2 Security Audit"
date: 2023-07-12T16:00:00+02:00
tags:
  - security
  - audit
  - opensource
---

Over the past year, the Eclipse Foundation has made securing the open source software supply chain a priority. By growing our security team and laying the groundwork for the Cyber Risk Initiative, we’ve made strides to improve the security posture of our open source projects.

Today, we’re taking another step forward with the completion of the [security audit](https://5413615.fs1.hubspotusercontent-na1.net/hubfs/5413615/OSTIF_-_Equinox_p2_Report.pdf) for Equinox p2, the provisioning component of the Eclipse IDE.

<!--more-->

[Equinox p2](https://eclipse.dev/equinox/p2/) was a logical choice for our first security audit because of its new signature verification mechanism. The plugin authentication mechanism incorporates PGP digital signatures, and is one of the new security features included in the [2023-06 Eclipse IDE release](https://eclipseide.org/). The existing mechanism for provisioning new plugins and extensions into the IDE to verify their signatures is an industry standard ([jar signing](https://docs.oracle.com/javase/tutorial/deployment/jar/signing.html)), but that is not the case for PGP digital signatures support.

This lack of assurance that the new mechanism was secure led us to order the audit, which was done in partnership with the [Open Source Technology Improvement Fund](https://ostif.org/) (OSTIF) and completed by [IncludeSecurity](https://includesecurity.com/). The audit revealed that a number of fixes were required, including providing users with more information so they can decide whether the extensions they are installing are safe.

All vulnerabilities that were identified, including one critical risk, have since been resolved. Check out the [full report](https://5413615.fs1.hubspotusercontent-na1.net/hubfs/5413615/OSTIF_-_Equinox_p2_Report.pdf) for more information.

Identifying and addressing vulnerabilities of any provisioning system through a security audit is a critically important aspect of supply chain security. In this case, these fixes will lower the risk of installing malware when developers obtain extensions from the internet.

The Eclipse IDE, and all extensible IDEs in the market, play a crucial role in the software supply chain. These are the tools used for writing, testing, and deploying software. If an IDE becomes infected with malware, significant damage to the downstream supply chain can follow.

We’re thrilled with the outcomes of the audit, and this proactive approach to security will save us time and effort in the long run.

This is the first time the Eclipse Foundation has funded a security audit for an Eclipse project, with three more audits in progress, and an additional three to be conducted later this year. The six upcoming audits are possible because of the funding the Eclipse Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project.

## Get Involved

* Join the [Eclipse IDE Working Group](https://eclipseide.org/working-group/) to help strengthen the IDE
* Learn more about the [Eclipse Cyber Risk Initiative](https://www.eclipse.org/org/workinggroups/eclipse-cyber-risk-concept.php), and how your organization can join the effort to strengthen the open source supply chain. Please subscribe to the [ECRI mailing list](https://accounts.eclipse.org/mailing-list/eclipse-cyber-risk-initiative) to join the initiative, or to follow its progress.
* Government regulation of the software industry is coming, and pending legislation in Europe such as the Cyber Resilience Act and the Product Liability Directive pose enormous risks to the [open source community and ecosystem](https://outreach.eclipse.foundation/cyber-resilience-act-open-source). We will be [holding an all members call](https://www.crowdcast.io/e/cra-july13/register) next **Thursday, July 13, at 14:30 CEST** to provide an update on the challenges we face as a community and as an industry.
