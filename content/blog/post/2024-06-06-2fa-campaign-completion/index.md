---
title: "Securing the Future: 2FA Now Mandatory for Eclipse Foundation Committers"
date: 2024-06-06T16:00:00+02:00
thumbnail: 2fa-pixel.png
tags:
  - security
  - opensource
  - 2fa
---

The Eclipse Foundation is pleased to announce the successful implementation of two-factor authentication (2FA) for all committers on both gitlab.eclipse.org and github.com. This initiative, aimed at bolstering the security of our source code repositories, mandates that all users with write access to an Eclipse Project repository (commonly known as committers) on GitHub and the [Eclipse Foundation GitLab instance](https://gitlab.eclipse.org) must use 2FA.

Two-factor authentication adds an extra layer of security by requiring not only a password but also a second form of verification. This significantly reduces the risk of unauthorized access and enhances the overall security of Eclipse Foundation projects.

The journey to this milestone has been extensive, but adoption has increased steadily over the past 18 months, aided by regular reminders and progressive enforcement.

![GitHub 2FA Adoption Rate](gh-2fa-adoption.png "Adoption percentage of 2FA by Eclipse Foundation Projects Committers over time on GitHub")

We deeply appreciate the cooperation of all our committers in enhancing the security of their repositories. Your commitment to maintaining high security standards is invaluable.

Looking ahead, our next project is to implement 2FA support for Eclipse Foundation accounts. Stay tuned for updates.

> This work was made possible by the funding the Eclipse Foundation received from the [Alpha-Omega](https://openssf.org/community/alpha-omega/) Project.
