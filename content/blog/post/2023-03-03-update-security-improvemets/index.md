---
title: "March 2023 Update on Security improvements at the Eclipse Foundation"
date: 2023-03-03T11:00:00+02:00
tags:
  - security
  - opensource
---

Thanks to [financial support](https://blogs.eclipse.org/post/mike-milinkovich/open-source-security-eclipse-foundation) from the [OpenSSF's Alpha-Omega project](https://openssf.org/community/alpha-omega/), the Eclipse Foundation is glad to have made significant improvements in the last couple of months.

<!--more-->

## Two Factor Authentication

[Eclipse Tycho](https://github.com/eclipse-tycho), [Eclipse m2e](https://github.com/eclipse-m2e), and [Eclipse RAP](https://github.com/eclipse-rap) have all enforced 2FA for all their committers on GitHub:
* https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2701
* https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2702
* https://gitlab.eclipse.org/eclipsefdn/helpdesk/-/issues/2611

Meanwhile, we've seen an increase of adoption of 2FA globally on all Eclipse Projects at Github, increasing from 63.7% to **67%** since the begining of the year. We are now starting to actively enforce 2FA for projects with a dedicated GitHub organization.

## Security Audits

We have successfully initiated the 3 security audits that will all be performed by [Trail of Bits](https://www.trailofbits.com/) in collaboration with [OSTIF](https://ostif.org/). The projects that will be covered in these audits are:

* [Eclipse Jetty](https://www.eclipse.org/jetty/): an open-source Java-based web server that provides a HTTP server and servlet container.
* [Eclipse JKube](https://www.eclipse.org/jkube/): a toolkit for building container images and deploying them to Kubernetes.
* [Eclipse Mosquito](https://mosquitto.org/): an open-source IoT platform that enables the development and management of connected devices.

Threat modeling for one out of the three audits have been completed. Code review is underway. The timeline has been locked in for threat modeling and code review for the second security audit. The schedule of the third one is still a work in progress, but will likely be delayed due to project's constraint. This last one will likely complete in May.

## Hiring

We have build capacity since the beginning of the year, [hiring 3 talented people](https://www.eclipse.org/org/foundation/staff.php):

* **Marta Rybczynska**, Technical Program Manager. They bring a wealth of experience and knowledge to the team. She initially focusing on improving security / vulnerability policies, procedures, and guidelines that adhere to industry best practices. She started early January.
* **Thomas Neidhart**, Software Engineer. He is initially focusing on SLSA attestation generation and GitHub management tooling. He started mid-January.
* **Francisco Perez**, Software Engineer. He will work closely with Eclipse Foundation Projects to enhance their software supply chain security. He started begining of March.

We're also in talks with a SecOps professional to improve the security of our infrastructure and introduce new tools and services, such as a self-hosted [sigstore](https://www.sigstore.dev).

## Rework of the CVE process

We have started gathering feedback from projects about Eclipse's security processes. We are performing interviews with committers and project leads, starting with projects selected for the audit or having a recent security vulnerability. We have contacted six projects, conducted four interviews, and gathered helpful feedback.

The common outcome is a request for more detailed documentation and clarification of the process. Proposals for updated documentations are currently under review. More interviews are planned. We've extended the experimentation of GitHub security advisories. We have also worked on a SECURITY.md template for all Eclipse Foundation projects.

## GitHub organizations and repositories management

We have re-started the work on a custom tool to enforce and create security related configurations of organizations and their associated repositories on GitHub. The tool is codenamed [OtterDog](https://gitlab.eclipse.org/eclipsefdn/security/otterdog/).

What is currently supported:
 * descriptive definition of required organization settings / webhooks, repositories and associated branch protection rules
 * mechanism to fetch the current configuration from an organization hosted on GitHub
 * verification on how the required configuration differs from the current live configuration hosted on GitHub
 * update mechanism to enforce the required configuration on GitHub

Some work has been done in order to improve the usability of the tool. The tool will output in a concise way what settings / resources will be changed / created prior to applying the configuration to GitHub by comparing the current live settings on GitHub to the intended configuration.

A credential provider for [pass](https://www.passwordstore.org) (in addition to existing one for [Bitwarden](https://bitwarden.com)) has been added to support using the tool for our first organization: [Eclipse CBI](https://github.com/eclipse-cbi/) which hosts various tools and projects for common build infrastructure at the Eclipse Foundation.

## SLSA tools
We started to work on [slsa-tools](https://gitlab.eclipse.org/netomi/slsa-tools) which is a collection of tools written in Java to operate on SLSA provenance files. The idea behind this project is to have a rich set of tools to verify / generate provenance files for the Java ecosystem.

Existing SLSA tools are implemented in Go which make it somewhat cumbersome to use them in certain settings, e.g. to develop a Jenkins plugin to generate provenance files for builds.

The medium-term goal is to develop such a Jenkins plugin with features similar to the existing [slsa-github-generator](https://github.com/slsa-framework/slsa-github-generator/) action for GitHub.
